package net.downwithdestruction.dwdcommands.listeners;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.commands.CmdSpawn;
import net.downwithdestruction.dwdcommands.configuration.PlayerConfig;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerListener implements Listener {
	private DwDCommands plugin;
	
	public PlayerListener(DwDCommands plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerLogin(PlayerLoginEvent event) {
		final Player player = event.getPlayer();
		if (player == null)
			return;
		final PlayerConfig pc = new PlayerConfig(player);
		if (!player.isBanned()) return;
		if (pc.get("bantime") != null && !Utils.isTimeStampValid(player, "bantime")) {
			player.setBanned(false);
			return;
		}
		String kickMessage = pc.getString("banreason");
		if (kickMessage == null) kickMessage = plugin.banMessage;
		event.setKickMessage(kickMessage);
		event.disallow(Result.KICK_BANNED, kickMessage);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerJoin(PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		if (player == null)
			return;
		final PlayerConfig pc = new PlayerConfig(player);
		if (pc.getString("name") == null) {
			plugin.debug("Creating userdata file for " + player.getName() + ".");
			pc.setString(player.getName(), "name");
			pc.setString(player.getAddress().getAddress().toString().replace("/", ""), "ipAddress");
			final String name = (player.getDisplayName() == null || player.getDisplayName().trim().equals("")) ? player.getName() : player.getDisplayName();
			pc.setString(name, "nickname");
			pc.setString("", "banreason");
			plugin.debug("Userdata creation finished.");
			if (plugin.useWelcome) {
				player.sendMessage(plugin.welcomeMessage.replace("{name}", name).replace("{world}", player.getWorld().getName()));
				plugin.getServer().broadcastMessage(plugin.broadcastWelcomeMessage.replace("{name}", name));
			}
			Utils.teleport(player, CmdSpawn.getWorldSpawn(player.getWorld()));
		} else {
			plugin.debug("Updating the IP for " + event.getPlayer().getName() + ".");
			pc.setString(event.getPlayer().getAddress().getAddress().toString().replace("/", ""), "ipAddress");
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		final Player player = e.getPlayer();
		final PlayerConfig pc = new PlayerConfig(player);
		pc.setLong((new Date()).getTime(), "seen");
	}
	
	@EventHandler
	public void onKick(PlayerKickEvent e) {
		final Player player = e.getPlayer();
		final PlayerConfig pc = new PlayerConfig(player);
		pc.setLong((new Date()).getTime(), "seen");
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerInteract(PlayerInteractEvent event) {
		final Player player = event.getPlayer();
		if (event.getAction().equals(Action.PHYSICAL))
			return;
		final ItemStack id = event.getItem();
		if (id == null)
			return;
		final int idn = id.getTypeId();
		if (idn == 0)
			return;
		final List<String> cmds = new PlayerConfig(player).getStringList("assign." + idn);
		if (cmds == null)
			return;
		for (String s : cmds) {
			if (s.toLowerCase().trim().startsWith("c:"))
				player.chat(s.trim().substring(2));
			else
				player.performCommand(s.trim());
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player p = event.getPlayer();
		PlayerConfig pc = new PlayerConfig(p);
		if (pc.getBoolean("muted")) {
			if (pc.get("mutetime") != null && !Utils.isTimeStampValid(p, "mutetime")) {
				pc.setBoolean(false, "muted");
			}
			event.setFormat("");
			event.setCancelled(true);
			p.sendMessage(ChatColor.RED + "You are muted.");
			plugin.log(p.getName() + " tried to speak, but has been muted.");
			return;
		}
		if (pc.get("ignoredby") == null) return;
		Set<Player> recpts = event.getRecipients();
		ArrayList<String> ignores = (ArrayList<String>) pc.getStringList("ignoredby");
		if (ignores == null) return;
		Set<Player> ignore = new HashSet<Player>();
		for (Player pl : recpts) {
			for (String ignoree : ignores) {
				if (pl.getName().equalsIgnoreCase(ignoree.toLowerCase())) ignore.add(pl);
			}
		}
		event.getRecipients().removeAll(ignore);
	}
	
	@EventHandler
	public void silentKicks(PlayerKickEvent e) {
		if (e.isCancelled()) return;
		String reason = e.getReason();
		if (!reason.endsWith("\00-silent")) return;
		e.setLeaveMessage(null);
		e.setReason(reason.replace("\00-silent", ""));
	}
	
	@EventHandler
	public void nickNames(PlayerLoginEvent e) {
		if (e.getResult() != Result.ALLOWED) return;
		Player p = e.getPlayer();
		if (p == null) return;
		String nick = new PlayerConfig(p).getString("nickname");
		if (nick == null || nick.equals("") || nick.equalsIgnoreCase(p.getName().trim())) return;
		String newnick = Utils.colorize(plugin.nickPrefix) + nick;
		p.setDisplayName(newnick);
		if (newnick.length() <= 16) p.setPlayerListName(newnick);
		else p.setPlayerListName(newnick.substring(0, 16));
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
		if (event.isCancelled()) return;
		Player p = event.getPlayer();
		if (plugin.showcommands) {
			plugin.log("[PLAYER_COMMAND] " + p.getName() + ": " + event.getMessage());
		}
		PlayerConfig pc = new PlayerConfig(p);
		if (pc.getBoolean("muted")) {
			if (pc.get("mutetime") != null && !Utils.isTimeStampValid(p, "mutetime")) {
				pc.setBoolean(false, "muted");
			}
			for (String command : plugin.muteCmds) {
				if (!(event.getMessage().toLowerCase().startsWith(command.toLowerCase() + " ") || event.getMessage().equalsIgnoreCase(command.toLowerCase()))) continue;
				p.sendMessage(ChatColor.RED + "You are muted.");
				plugin.log(p.getName() + " tried to use that command, but is muted.");
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void lastLocation(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		PlayerConfig pc = new PlayerConfig(p);
		Location l = p.getLocation();
		pc.setString(l.getWorld().getName(), "lastlocation.world");
		pc.setDouble(l.getX(), "lastlocation.x");
		pc.setDouble(l.getY(), "lastlocation.y");
		pc.setDouble(l.getZ(), "lastlocation.z");
		pc.setFloat(l.getYaw(), "lastlocation.yaw");
		pc.setFloat(l.getPitch(), "lastlocation.pitch");
	}
}
