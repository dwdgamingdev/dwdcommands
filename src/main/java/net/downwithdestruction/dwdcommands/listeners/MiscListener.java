package net.downwithdestruction.dwdcommands.listeners;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class MiscListener implements Listener {
	private DwDCommands plugin;
	
	public MiscListener(DwDCommands plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPing(final ServerListPingEvent event) {
		event.setMotd(Utils.colorize(plugin.currentServerTitle));
	}
}
