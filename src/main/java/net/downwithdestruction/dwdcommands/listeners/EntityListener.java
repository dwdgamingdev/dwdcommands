package net.downwithdestruction.dwdcommands.listeners;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.configuration.PlayerConfig;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class EntityListener implements Listener {
	//private DwDCommands plugin;
	
	public EntityListener(DwDCommands plugin) {
		//this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDeath(EntityDeathEvent event) {
		if (!(event instanceof PlayerDeathEvent))
			return;
		final PlayerDeathEvent e = (PlayerDeathEvent) event;
		if (e.getEntity() == null)
			return;
		final Player player = e.getEntity();
		if (Utils.isAuthorized(player, "dwdcommands.back")) {
			player.sendMessage(ChatColor.LIGHT_PURPLE + "Type " + ChatColor.GRAY + "/back" + ChatColor.LIGHT_PURPLE + " to go back to where you died.");
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void buddhaMode(EntityDamageEvent event) {
		if (!(event.getEntity() instanceof Player))
			return;
		final Player player = (Player) event.getEntity();
		if (!new PlayerConfig(player).getBoolean("buddha"))
			return;
		if (event.getDamage() >= player.getHealth())
			event.setDamage(player.getHealth() - 1);
		if (player.getHealth() <= 2) {
			player.setHealth(2);
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void godModeHealth(EntityDamageEvent event) {
		if (!(event.getEntity() instanceof Player))
			return;
		final Player player = (Player) event.getEntity();
		if (new PlayerConfig(player).getBoolean("godmode")) {
			player.setHealth(20);
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void godModeHunger(FoodLevelChangeEvent event) {
		if (!(event.getEntity() instanceof Player))
			return;
		final Player player = (Player) event.getEntity();
		if (new PlayerConfig(player).getBoolean("godmode")) {
			event.setFoodLevel(20);
			player.setSaturation(20F);
		}
	}
}
