package net.downwithdestruction.dwdcommands.configuration;

import java.io.File;
import java.io.IOException;
import java.util.List;

import net.downwithdestruction.dwdcommands.utils.Logger;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public abstract class Configuration {
	private final Logger logger = Logger.getInstance();
	private FileConfiguration config;
	private File file;

	/**
	 * Constructs a Configuration
	 * @param file to configuration file desired.
	 */
	public Configuration(File file) {
		this.file = file;
	}

	public Configuration(String path) {
		this.file = new File(path);
	}

	/**
	 * Returns the associated YML file.
	 * @return the configuration file.
	 */
	public File getFile() {
		return file;
	}

	/**
	 * Returns the associated FileConfiguration
	 */
	public FileConfiguration getConfig() {
		return config;
	}

	/**
	 * Sets where the configuration file is located.
	 * @param file to set.
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * Loads the configuration file to memory. The file is created if missing.
	 */
	public void load() {
		try {
			// Create our file if it doesn't exist
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			config = YamlConfiguration.loadConfiguration(file);
			logger.config("Loaded configuration file at " + file.getAbsolutePath());
		} catch (IOException e) {
			logger.severe("Config file at " + file.getAbsolutePath() + " failed to load: " + e.getMessage());
		}
	}

	/**
	 * Saves the configuration in memory to disk.
	 */
	public void save() {
		try {
			config.save(file);
			logger.config("Saved config file at: " + file.getAbsolutePath());
		} catch (IOException e) {
			logger.severe("Config file at " + file.getAbsolutePath() + " failed to save: " + e.getMessage());
		}
	}
	
	/**
	 * Convenience Methods
	 */
	public boolean isBoolean(String path) {
		return config.isBoolean(path);
	}
	
	public void setString(String value, String path) {
		if (config == null) return;
		config.set(path, value);
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setLong(long value, String path) {
		if (config == null) return;
		config.set(path, value);
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setDouble(double value, String path) {
		if (config == null) return;
		config.set(path, value);
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void set(Object value, String path) {
		if (config == null) return;
		config.set(path, value);
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setInteger(Integer value, String path) {
		if (config == null) return;
		config.set(path, value);
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setStringList(List<String> value, String path) {
		if (config == null) return;
		config.set(path, value);
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setBoolean(Boolean value, String path) {
		if (config == null) return;
		config.set(path, value);
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setFloat(Float value, String path) {
		if (config == null) return;
		config.set(path, value);
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public List<String> getStringList(String path) {
		if (config == null) return null;
		return config.getStringList(path);
	}
	
	public boolean getBoolean(String path) {
		return config != null && config.getBoolean(path);
	}
	
	public Integer getInteger(String path) {
		if (config == null) return null;
		return config.getInt(path);
	}
	
	public Object get(String path) {
		if (config == null) return null;
		return config.get(path);
	}
	
	public Long getLong(String path) {
		if (config == null) return null;
		return config.getLong(path);
	}
	
	public Double getDouble(String path) {
		if (config == null) return null;
		return config.getDouble(path);
	}
	
	public String getString(String path) {
		if (config == null) return null;
		return config.getString(path);
	}
	
	public Float getFloat(String path) {
		if (config == null) return null;
		try {
			return Float.valueOf(config.getString(path));
		} catch (Exception e) {
			return null;
		}
	}
	
	public ConfigurationSection getConfigurationSection(String path) {
		if (config == null) return null;
		return config.getConfigurationSection(path);
	}
	
	public boolean getConfExists() {
		return config != null;
	}
	
	public boolean exists() {
		return config != null;
	}
}
