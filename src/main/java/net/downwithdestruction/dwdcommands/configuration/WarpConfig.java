package net.downwithdestruction.dwdcommands.configuration;

import java.io.File;
import net.downwithdestruction.dwdcommands.DwDCommands;

public class WarpConfig extends Configuration {

    public WarpConfig(String name) {
        super(new File(DwDCommands.getInstance().getDataFolder() + File.separator + "warps" + File.separator + name + ".yml"));
        load();
    }
}
