package net.downwithdestruction.dwdcommands.configuration;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Logger;

public class MainConfig extends Configuration {
	private final static Logger logger = Logger.getInstance();
	
	public MainConfig() {
		super(new File(DwDCommands.getInstance().getDataFolder() + File.separator + "config.yml"));
		load();
	}
	
	@Override
	public void load() {
		File file = super.getFile();
		try {
			// Create our file if it doesn't exist
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
				FileConfiguration config = YamlConfiguration.loadConfiguration(file);
				config.addDefault("enable-welcome-message", true);
				config.addDefault("welcome-message", "&aWelcome to DwD, {name}! You are in the world [{world}].");
				config.addDefault("broadcast-welcome-message", "&aNew player {name} has joined. Please welcome them to the server!");
				config.addDefault("default-server-title", "A Minecraft Server");
				config.addDefault("current-server-title", "A Minecraft Server");
				config.addDefault("kick-message", "You have been kicked");
				config.addDefault("ban-message", "The ban-hammer has spoken!");
				config.addDefault("log-all-commands", true);
				config.addDefault("warp-last-location", true);
				config.addDefault("nick-prefix", "|");
				config.addDefault("afk-kick-time", 120L);
				config.addDefault("afk-auto-time", 300L);
				config.addDefault("tag-refresh-rate", 3);
				config.addDefault("use-exclusive-kit-perms", false);
				config.addDefault("home-limits.groups.Mod", 5);
				config.addDefault("home-limits.groups.Admin", 10);
				config.addDefault("mute-blocked-commands", new String[] {
					"/me",
					"/msg"
				});
				config.addDefault("kits.help.items", new String[] {
					"17:25",// 25 Wood Logs
					"20:10",// 10 Glass
					"35:3", // 3 White Wool
					"4:64", // 64 Cobble Stone
					"46:2", // 2 TNT
					"50:6", // 6 Torch
					"54:1", // 1 Chest
					"275:1",// 1 Stone Axe
					"295:4",// 4 Seeds
					"320:1",// 1 Cooked Ham (Porkchop)
					"364:1" // 1 Cooked Beef (Steak)
				});
				config.addDefault("kits.help.cooldown", 604800);
				config.options().copyDefaults(true);
				config.save(file);
				logger.config("Created new default configuration file: " + file.getAbsolutePath());
			}
		} catch (IOException e) {
			logger.severe("Failed to create new default config: " + file.getAbsolutePath() + ". " + e.getMessage());
		}
		// Now load the file up into memory.
		super.load();
	}
}
