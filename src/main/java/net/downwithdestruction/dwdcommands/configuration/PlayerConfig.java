package net.downwithdestruction.dwdcommands.configuration;

import java.io.File;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Logger;

public class PlayerConfig extends Configuration {
	private final Logger logger = Logger.getInstance();
	
	public PlayerConfig(OfflinePlayer player) {
		super(new File(DwDCommands.getInstance().getDataFolder() + File.separator + "userdata" + File.separator + player.getName().toLowerCase() + ".yml"));
		chkUserDir();
		this.load();
	}
	
	public PlayerConfig(Player player) {
		super(new File(DwDCommands.getInstance().getDataFolder() + File.separator + "userdata" + File.separator + player.getName().toLowerCase() + ".yml"));
		chkUserDir();
		this.load();
	}
	
	public PlayerConfig(String player) {
		super(new File(DwDCommands.getInstance().getDataFolder() + File.separator + "userdata" + File.separator + player.toLowerCase() + ".yml"));
		chkUserDir();
		this.load();
	}
	
	private void chkUserDir() {
		// Make userdata directory if it doesnt exist.
		File dir = new File(DwDCommands.getInstance().getDataFolder() + File.separator + "userdata" + File.separator);
		if (!dir.exists()) {
			try {
				if (dir.mkdir()) {
					logger.info("Created userdata directory.");
				}
			} catch (Exception e) {
				logger.severe("Failed to make userdata directory!");
				logger.severe(e.getMessage());
			}
		}
	}
}
