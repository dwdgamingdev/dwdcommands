package net.downwithdestruction.dwdcommands.configuration;

import java.io.File;

import net.downwithdestruction.dwdcommands.DwDCommands;

public class SpawnConfig extends Configuration {
	public SpawnConfig() {
		super(new File(DwDCommands.getInstance().getDataFolder() + File.separator + "spawns.yml"));
		load();
	}
}
