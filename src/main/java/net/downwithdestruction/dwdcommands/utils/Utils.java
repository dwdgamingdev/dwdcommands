package net.downwithdestruction.dwdcommands.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.configuration.PlayerConfig;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.RemoteConsoleCommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Utils {
	private final static Logger logger = Logger.getInstance();
	
	public static final Set<Integer> AIR_MATERIALS = new HashSet<Integer>();
	public static final HashSet<Byte> AIR_MATERIALS_TARGET = new HashSet<Byte>();

	static {
		AIR_MATERIALS.add(Material.AIR.getId());
		AIR_MATERIALS.add(Material.SAPLING.getId());
		AIR_MATERIALS.add(Material.POWERED_RAIL.getId());
		AIR_MATERIALS.add(Material.DETECTOR_RAIL.getId());
		AIR_MATERIALS.add(Material.LONG_GRASS.getId());
		AIR_MATERIALS.add(Material.DEAD_BUSH.getId());
		AIR_MATERIALS.add(Material.YELLOW_FLOWER.getId());
		AIR_MATERIALS.add(Material.RED_ROSE.getId());
		AIR_MATERIALS.add(Material.BROWN_MUSHROOM.getId());
		AIR_MATERIALS.add(Material.RED_MUSHROOM.getId());
		AIR_MATERIALS.add(Material.TORCH.getId());
		AIR_MATERIALS.add(Material.REDSTONE_WIRE.getId());
		AIR_MATERIALS.add(Material.SEEDS.getId());
		AIR_MATERIALS.add(Material.SIGN_POST.getId());
		AIR_MATERIALS.add(Material.WOODEN_DOOR.getId());
		AIR_MATERIALS.add(Material.LADDER.getId());
		AIR_MATERIALS.add(Material.RAILS.getId());
		AIR_MATERIALS.add(Material.WALL_SIGN.getId());
		AIR_MATERIALS.add(Material.LEVER.getId());
		AIR_MATERIALS.add(Material.STONE_PLATE.getId());
		AIR_MATERIALS.add(Material.IRON_DOOR_BLOCK.getId());
		AIR_MATERIALS.add(Material.WOOD_PLATE.getId());
		AIR_MATERIALS.add(Material.REDSTONE_TORCH_OFF.getId());
		AIR_MATERIALS.add(Material.REDSTONE_TORCH_ON.getId());
		AIR_MATERIALS.add(Material.STONE_BUTTON.getId());
		AIR_MATERIALS.add(Material.SUGAR_CANE_BLOCK.getId());
		AIR_MATERIALS.add(Material.DIODE_BLOCK_OFF.getId());
		AIR_MATERIALS.add(Material.DIODE_BLOCK_ON.getId());
		AIR_MATERIALS.add(Material.TRAP_DOOR.getId());
		AIR_MATERIALS.add(Material.PUMPKIN_STEM.getId());
		AIR_MATERIALS.add(Material.MELON_STEM.getId());
		AIR_MATERIALS.add(Material.VINE.getId());
		AIR_MATERIALS.add(Material.NETHER_WARTS.getId());
		AIR_MATERIALS.add(Material.WATER_LILY.getId());
		AIR_MATERIALS.add(Material.SNOW.getId());

		for (Integer integer : AIR_MATERIALS) {
			AIR_MATERIALS_TARGET.add(integer.byteValue());
		}
		AIR_MATERIALS_TARGET.add((byte) Material.WATER.getId());
		AIR_MATERIALS_TARGET.add((byte) Material.STATIONARY_WATER.getId());
	}
	
	public static boolean isAuthorized(final CommandSender player, final String node) {
		return  player instanceof RemoteConsoleCommandSender ||
				player instanceof ConsoleCommandSender ||
				player.hasPermission("dwdcommands.op") ||
				player.hasPermission(node);
	}
	
	public static void dispNoPerms(CommandSender sender) {
		sender.sendMessage(ChatColor.RED + "You don't have permission for that!");
		logger.warn(sender.getName() + " was denied access to that!");
	}
	
	public static void dispNoPerms(CommandSender sender, String message) {
		sender.sendMessage(message);
		logger.warn(sender.getName() + " was denied access to that!");
	}
	
	public static ItemStack getItem(String name, Integer amount) {
		Short data;
		String datas = null;
		name = name.trim().toUpperCase();
		if (name.contains(":")) {
			if (name.split(":").length < 2) {
				datas = null;
				name = name.split(":")[0];
			} else {
				datas = name.split(":")[1];
				name = name.split(":")[0];
			}
		}
		try {
			data = Short.valueOf(datas);
		} catch (Exception e) {
			data = null;
		}
		Material mat = Material.getMaterial(name);
		if (mat == null) {
			try {
				mat = Material.getMaterial(Integer.valueOf(name));
				if (mat == null) return null;
			} catch (Exception e) {
				return null;
			}
		}
		if (amount == null) amount = 1;
		ItemStack stack = new ItemStack(mat, amount);
		if (data != null) stack.setDurability(data);
		return stack;
	}
	
	public static String getItemName(ItemStack is) {
		return is.getType().name().toLowerCase().replace("_", " ");
	}
	
	public static String getItemName(Material m) {
		return m.name().toLowerCase().replace("_", " ");
	}
	
	public static Double getDouble(String number) {
		try {
			return Double.valueOf(number);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static void teleport(Player p, Location l) {
		p.teleport(l);
	}
	
	public static void teleport(Player p, Entity e) {
		p.teleport(e);
	}
	
	public static String formatDateDiff(long date) {
		Calendar c = new GregorianCalendar();
		c.setTimeInMillis(date);
		Calendar now = new GregorianCalendar();
		return formatDateDiff(now, c);
	}
	
	private static int dateDiff(int type, Calendar fromDate, Calendar toDate, boolean future) {
		int diff = 0;
		long savedDate = fromDate.getTimeInMillis();
		while ((future && !fromDate.after(toDate)) || (!future && !fromDate.before(toDate))) {
			savedDate = fromDate.getTimeInMillis();
			fromDate.add(type, future ? 1 : -1);
			diff++;
		}
		diff--;
		fromDate.setTimeInMillis(savedDate);
		return diff;
	}
	
	public static String formatDateDiff(Calendar fromDate, Calendar toDate) {
		boolean future = false;
		if (toDate.equals(fromDate)) return " now";
		if (toDate.after(fromDate)) future = true;
		StringBuilder sb = new StringBuilder();
		int[] types = new int[] {
			Calendar.YEAR,
			Calendar.MONTH,
			Calendar.DAY_OF_MONTH,
			Calendar.HOUR_OF_DAY,
			Calendar.MINUTE,
			Calendar.SECOND
		};
		String[] names = new String[] {
			"year",
			"years",
			"month",
			"months",
			"day",
			"days",
			"hour",
			"hours",
			"minute",
			"minutes",
			"second",
			"seconds"
		};
		for (int i = 0; i < types.length; i++) {
			int diff = dateDiff(types[i], fromDate, toDate, future);
			if (diff > 0) sb.append(" ").append(diff).append(" ").append(names[i * 2 + (diff > 1 ? 1 : 0)]);
		}
		if (sb.length() == 0) return " now";
		return sb.toString();
	}
	
	public static String getFinalArg(final String[] args, final int start) {
		 final StringBuilder bldr = new StringBuilder();
		 for (int i = start; i < args.length; i++) {
			 if (i != start) {
				 bldr.append(" ");
			 }
			 bldr.append(args[i]);
		 }
		 return bldr.toString();
	}
	
	public static Block getTarget(Player p) {
		return p.getTargetBlock(AIR_MATERIALS_TARGET, 300);
	}
	
	public static boolean isTeleportAllowed(OfflinePlayer p) {
		PlayerConfig pc = new PlayerConfig(p);
		return pc.get("teleportenabled") == null || pc.getBoolean("teleportenabled");
	}
	
	public static int getNumberVanished() {
		int hid = 0;
		for (Player p : DwDCommands.getInstance().getServer().getOnlinePlayers()) if (DwDCommands.getInstance().isVanished(p)) hid++;
		return hid;
	}
	
	public static String colorize(String text) {
		if (text == null) return null;
		return text.replaceAll("(&([a-f0-9k-or]))", "\u00A7$2");
	}
	
	public static void scheduleKick(final Player p, final String reason) throws IllegalArgumentException, NullPointerException {
		if (p == null) throw new IllegalArgumentException("Player cannot be null!");
		if (reason == null) throw new IllegalArgumentException("Reason cannot be null!");
		Runnable r = new Runnable() {
			@Override
			public void run() {
				p.kickPlayer(reason);
			}
		};
		if (DwDCommands.getInstance() == null)
			throw new NullPointerException("Could not get the DwDCommands plugin.");
		DwDCommands.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(DwDCommands.getInstance(), r);
	}
	
	public static boolean isTimeStampValid(OfflinePlayer p, String title) {
		PlayerConfig pc = new PlayerConfig(p);
		if (pc.get(title) == null) return false;
		long time = new Date().getTime();
		long overall = pc.getLong(title);
		return time < overall;
	}
	
	public static void setTimeStamp(OfflinePlayer p, long seconds, String title) {
		PlayerConfig pc = new PlayerConfig(p);
		pc.setLong((seconds * 1000) + new Date().getTime(), title);
	}
	
	public static long getTimeStamp(OfflinePlayer p, String title) {
		PlayerConfig pc = new PlayerConfig(p);
		if (pc.get(title) == null) return -1;
		return pc.getLong(title);
	}
	
	public static void silentKick(final Player t, final String reason) {
		t.kickPlayer(reason + "\00-silent");
	}
	
	public static boolean isValidIP(String address) {
		if (address == null) return false;
		String[] ips = address.split("\\.");
		if (ips.length != 4) return false;
		for (String s : ips) {
			int ip;
			try {
				ip = Integer.valueOf(s);
			} catch (Exception e) {
				return false;
			}
			if ((ip < 0) || (ip > 255)) return false;
		}
		return true;
	}
	
	public static boolean isIgnored(Player player, Player ignore) {
		return checkIfIgnored(new PlayerConfig(player), ignore.getName());
	}
	
	public static boolean isIgnored(Player player, OfflinePlayer ignore) {
		return checkIfIgnored(new PlayerConfig(player), ignore.getName());
	}
	
	public static boolean isIgnored(OfflinePlayer player, Player ignore) {
		return checkIfIgnored(new PlayerConfig(player), ignore.getName());
	}
	
	public static boolean isIgnored(OfflinePlayer player, OfflinePlayer ignore) {
		return checkIfIgnored(new PlayerConfig(player), ignore.getName());
	}
	
	private static boolean checkIfIgnored(PlayerConfig pc, String ignore) {
		if (pc.get("ignoredby") == null) return false;
		ArrayList<String> ignores = (ArrayList<String>) pc.getStringList("ignoredby");
		if (ignores == null) return false;
		for (String ignoree : ignores) { if (ignore.equalsIgnoreCase(ignoree.toLowerCase())) return true; }
		return false;
	}
}
