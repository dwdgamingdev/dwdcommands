package net.downwithdestruction.dwdcommands.runners;

public abstract class TeleportLastLocation<Player> implements Runnable {
	private Player player;
	
	public TeleportLastLocation(Player player) {
		this.player = player;
	}
	
	@Override
	public final void run() {
		run(player);
	}
	
	public abstract void run(Player player);
}
