package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

public class CmdTpPos implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("tppos")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.tppos")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This message is only available to players!");
				return true;
			}
			if (args.length < 3) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			Double x = Utils.getDouble(args[0]);
			Double y = Utils.getDouble(args[1]);
			Double z = Utils.getDouble(args[2]);
			if (x == null || y == null || z == null) {
				cs.sendMessage(ChatColor.RED + "One of the coordinates was invalid.");
				return true;
			}
			Player p = (Player) cs;
			Location pLoc;
			World w = (args.length > 3) ? DwDCommands.getInstance().getServer().getWorld(args[3]) : p.getWorld();
			if (w == null) {
				cs.sendMessage(ChatColor.RED + "That world does not exist!");
				return true;
			}
			pLoc = new Location(w, x, y, z);
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "Teleporting you to x: " + ChatColor.GRAY + x + ChatColor.LIGHT_PURPLE + ", y: " + ChatColor.GRAY + y + ChatColor.LIGHT_PURPLE + ", z: " + ChatColor.GRAY + z + ChatColor.LIGHT_PURPLE + " in world " + ChatColor.GRAY + w.getName() + ChatColor.LIGHT_PURPLE + ".");
			Utils.teleport(p, pLoc);
			return true;
		}
		return false;
	}
}
