package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdRageQuit implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("ragequit")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.ragequit")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 1) {
				if (cs instanceof Player) {
					DwDCommands.getInstance().getServer().broadcastMessage(ChatColor.DARK_RED + ((Player) cs).getDisplayName() + ChatColor.RED + " has ragequit!");
					((Player) cs).kickPlayer(ChatColor.DARK_RED + "RAAAGGGEEEE!!!");
					return true;
				}
			}
			if (args.length == 1) {
				if (!Utils.isAuthorized(cs, "dwdcommands.others.ragequit")) {
					cs.sendMessage(ChatColor.RED + "You don't have permission for that!");
					return true;
				}
				Player t = DwDCommands.getInstance().getServer().getPlayer(args[0]);
				if (t == null || DwDCommands.getInstance().isVanished(t, cs)) {
					cs.sendMessage(ChatColor.RED + "That player does not exist!");
					return true;
				}
				if (Utils.isAuthorized(t, "dwdcommands.exempt.ragequit")) {
					cs.sendMessage(ChatColor.RED + "You cannot ragequit that user!");
					return true;
				}
				DwDCommands.getInstance().getServer().broadcastMessage(ChatColor.DARK_RED + t.getDisplayName() + ChatColor.RED + " has ragequit!");
				Utils.silentKick(t, ChatColor.DARK_RED + "RAAAGGGEEEE!!!");
				return true;
			}
			return true;
		}
		return false;
	}
}
