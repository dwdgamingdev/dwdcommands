package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

public class CmdFeed implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("feed")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.feed")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 1) {
				if (!(cs instanceof Player)) {
					cs.sendMessage(ChatColor.RED + "You can't feed the console!");
					return true;
				}
				Player t = (Player) cs;
				t.sendMessage(ChatColor.LIGHT_PURPLE + "You have fed yourself!");
				t.setFoodLevel(20);
				t.setSaturation(20);
				return true;
			}
			Player t = DwDCommands.getInstance().getServer().getPlayer(args[0]);
			if (!Utils.isAuthorized(cs, "dwdcommands.others.feed")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
				cs.sendMessage(ChatColor.RED + "That player does not exist!");
				return true;
			}
			if (Utils.isAuthorized(t, "dwdcommands.exempt.feed")) {
				cs.sendMessage(ChatColor.RED + "You cannot feed that player.");
				return true;
			}
			String name = (cs instanceof Player) ? ((Player) cs).getDisplayName() : "CONSOLE";
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "You have fed " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
			t.sendMessage(ChatColor.LIGHT_PURPLE + "You have been fed by " + ChatColor.GRAY + name + ChatColor.LIGHT_PURPLE + "!");
			t.setFoodLevel(20);
			t.setSaturation(20);
			return true;
		}
		return false;
	}
}
