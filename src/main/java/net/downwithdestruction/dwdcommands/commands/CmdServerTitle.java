package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.configuration.MainConfig;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdServerTitle implements CommandExecutor {
	private final static DwDCommands plugin = DwDCommands.getInstance();
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("clear")) {
				if (!Utils.isAuthorized(cs, "dwdcommands.servertitle.clear")) {
					Utils.dispNoPerms(cs);
					return true;
				}
				MainConfig conf = new MainConfig();
				plugin.currentServerTitle = plugin.defaultServerTitle;
				conf.set("current-server-title", plugin.currentServerTitle);
				cs.sendMessage(ChatColor.GREEN + "Server title successfully set back to the default: " + ChatColor.RESET + Utils.colorize(plugin.defaultServerTitle));
				return true;
			}
			if (args[0].equalsIgnoreCase("set")) {
				if (!Utils.isAuthorized(cs, "dwdcommands.servertitle.set")) {
					Utils.dispNoPerms(cs);
					return true;
				}
				if (args.length < 2) {
					cs.sendMessage(ChatColor.RED + "You must put a title to set!");
					return true;
				}
				String title = Utils.getFinalArg(args, 1);
				MainConfig conf = new MainConfig();
				plugin.currentServerTitle = title;
				conf.set("current-server-title", plugin.currentServerTitle);
				cs.sendMessage(ChatColor.GREEN + "Server title successfully set to: " + ChatColor.RESET + Utils.colorize(title));
				return true;
			}
		}
		if (!Utils.isAuthorized(cs, "dwdcommands.servertitle.view")) {
			Utils.dispNoPerms(cs);
			return true;
		}
		cs.sendMessage(ChatColor.GREEN + "Current Server Title: " + ChatColor.RESET + Utils.colorize(plugin.currentServerTitle));
		return true;
	}
}
