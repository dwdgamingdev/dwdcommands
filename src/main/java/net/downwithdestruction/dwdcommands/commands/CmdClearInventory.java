package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

public class CmdClearInventory implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("clearinventory")) {
			if (!Utils.isAuthorized(sender, "dwdcommands.clearinventory")) {
				Utils.dispNoPerms(sender);
				return true;
			}
			if (args.length < 1) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(cmd.getDescription());
					return false;
				}
				Player p = (Player) sender;
				p.getInventory().clear();
				sender.sendMessage(ChatColor.LIGHT_PURPLE + "You have cleared your inventory.");
				return true;
			}
		}
		if (args.length == 1) {
			if (!Utils.isAuthorized(sender, "dwdcommands.others.clearinventory")) {
				Utils.dispNoPerms(sender);
				return true;
			}
			Player t = DwDCommands.getInstance().getServer().getPlayer(args[0].trim());
			if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
				sender.sendMessage(ChatColor.RED + "That player is not online!");
				return true;
			}
			sender.sendMessage(ChatColor.LIGHT_PURPLE + "You have cleared the inventory of " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
			t.sendMessage(ChatColor.RED + "Your inventory has been cleared.");
			t.getInventory().clear();
			return true;
		}
		return false;
	}
}
