package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdHarm implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("harm")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.harm")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			Player t = DwDCommands.getInstance().getServer().getPlayer(args[0].trim());
			if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
				cs.sendMessage(ChatColor.RED + "That player is not online.");
				return true;
			}
			int toDamage;
			if (args.length > 1) {
				try {
					toDamage = Integer.parseInt(args[1]);
				} catch(NumberFormatException e) {
					cmd.getDescription();
					cs.sendMessage(ChatColor.RED + "Damage must be a number between 1 and 20.");
					return false;
				}
				if ((toDamage > 20) || (toDamage < 1)) {
					cs.sendMessage(ChatColor.RED + "The damage you entered is not between 1 and 20.");
					return true;
				}
			} else {
				toDamage = 5; // default damage if not specified
			}
			if (Utils.isAuthorized(t, "dwdcommands.exempt.harm")) {
				cs.sendMessage(ChatColor.RED + "You cannot harm that player.");
				return true;
			}
			t.damage(toDamage);
			t.sendMessage(ChatColor.LIGHT_PURPLE + "You have just been harmed by " + ChatColor.GRAY + ((Player) cs).getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "You just harmed " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
			return true;
		}
		return false;
	}
}
