package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdKillAll implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("killall")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.killall")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			Player[] ps = DwDCommands.getInstance().getServer().getOnlinePlayers();
			for (Player p : ps) {
				if (DwDCommands.getInstance().isVanished(p, cs) || Utils.isAuthorized(p, "dwdcommands.exempt.kill"))
					continue;
				if (cs instanceof Player) {
					if (p == cs) continue;
				}
				p.setHealth(0);
			}
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "You have killed all the players.");
			return true;
		}
		return false;
	}
}
