package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

public class CmdHeal implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("heal")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.heal")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 1) {
				if (!(cs instanceof Player)) {
					cs.sendMessage(cmd.getDescription());
					return false;
				}
				Player t = (Player) cs;
				t.sendMessage(ChatColor.LIGHT_PURPLE + "You have healed yourself!");
				t.setHealth(20);
				return true;
			}
			Player t = DwDCommands.getInstance().getServer().getPlayer(args[0].trim());
			if (!Utils.isAuthorized(cs, "dwdcommands.others.heal")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
				cs.sendMessage(ChatColor.RED + "That player does not exist!");
				return true;
			}
			if (Utils.isAuthorized(t, "dwdcommands.exempt.heal")) {
				cs.sendMessage(ChatColor.RED + "You cannot heal that player.");
				return true;
			}
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "You have healed " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
			t.sendMessage(ChatColor.LIGHT_PURPLE + "You have been healed by " + ChatColor.GRAY + ((Player) cs).getDisplayName() + ChatColor.LIGHT_PURPLE + "!");
			t.setHealth(20);
			return true;
		}
		return false;
	}
}
