package net.downwithdestruction.dwdcommands.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;
import org.apache.commons.lang.text.StrBuilder;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdList implements CommandExecutor {

    public String getNumOnline(CommandSender cs) {
        ChatColor CCLP = ChatColor.LIGHT_PURPLE;
        ChatColor CCG = ChatColor.GRAY;
        int maxP = DwDCommands.getInstance().getServer().getMaxPlayers();
        int hid = Utils.getNumberVanished();
        int all = DwDCommands.getInstance().getServer().getOnlinePlayers().length;
        boolean canSeeVanished = Utils.isAuthorized(cs, "dwdcommands.seehidden");
        String numPlayers;
        if (canSeeVanished && hid > 0) {
            numPlayers = (all - hid) + "/" + hid;
        } else {
            numPlayers = String.valueOf(all - hid);
        }
        return CCLP + "There are currently " + CCG + numPlayers + CCLP + " out of " + CCG + maxP + CCLP + " players online.";
    }

    public static String[] getGroupList(CommandSender cs) {
        ChatColor CCG = ChatColor.GRAY;
        ChatColor CCRT = ChatColor.RESET;
        Player[] pl = DwDCommands.getInstance().getServer().getOnlinePlayers();
        Map<String, List<String>> groups = new HashMap<String, List<String>>();
        StrBuilder sb = new StrBuilder();
        for (Player p : pl) {
            DwDCommands.getInstance();
            String group = DwDCommands.permission.getPrimaryGroup(p);
            List<String> inGroup = (groups.containsKey(group)) ? groups.get(group) : new ArrayList<String>();
            if (DwDCommands.getInstance().isVanished(p) && Utils.isAuthorized(cs, "dwdcommands.seehidden")) {
                inGroup.add(CCG + "[H]" + CCRT + p.getDisplayName());
            } else if (!DwDCommands.getInstance().isVanished(p)) {
                inGroup.add(p.getDisplayName());
            }
            groups.put(group, inGroup);
        }
        List<String> toRet = new ArrayList<String>();
        for (String group : groups.keySet()) {
            List<String> inGroup = groups.get(group);
            if (inGroup.size() < 1) {
                continue;
            }
            sb.append(group);
            sb.append(CCRT);
            sb.append(": ");
            for (String name : inGroup) {
                sb.append(name);
                sb.append(CCRT);
                sb.append(", ");
            }
            if (sb.length() < 2) {
                sb.clear();
                continue;
            }
            toRet.add(sb.toString().substring(0, sb.length() - 2));
            sb.clear();
        }
        for (String s : toRet) {
            if (s == null) {
                toRet.remove(s);
            }
        }
        return toRet.toArray(new String[toRet.size()]);
    }

    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("list")) {
            if (!Utils.isAuthorized(cs, "dwdcommands.list")) {
                Utils.dispNoPerms(cs);
                return true;
            }
            cs.sendMessage(getNumOnline(cs));
            cs.sendMessage(getGroupList(cs));
            return true;
        }
        return false;
    }
}
