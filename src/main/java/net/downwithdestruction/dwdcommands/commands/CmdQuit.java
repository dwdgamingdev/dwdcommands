package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdQuit implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("quit")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.quit")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) cs;
			Utils.silentKick(p, "You have left the game.");
			DwDCommands.getInstance().getServer().broadcastMessage(ChatColor.GRAY + p.getDisplayName() + ChatColor.LIGHT_PURPLE + " has left the game.");
			return true;
		}
		return false;
	}
}
