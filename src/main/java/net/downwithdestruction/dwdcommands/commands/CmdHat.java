package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class CmdHat implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("hat")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.hat")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) cs;
			if (args.length > 0 && (args[0].contains("rem") || args[0].contains("off") || args[0].equalsIgnoreCase("0"))) {
				final PlayerInventory inv = p.getInventory();
				final ItemStack head = inv.getHelmet();
				if (head == null || head.getType() == Material.AIR) {
					p.sendMessage(ChatColor.RED + "You are not wearing a hat.");
				} else {
					final ItemStack air = new ItemStack(Material.AIR);
					inv.setHelmet(air);
					p.getInventory().addItem(head);
					p.sendMessage(ChatColor.LIGHT_PURPLE + "Your hat has been removed.");
				}
			} else {
				if (p.getItemInHand().getType() != Material.AIR) {
					final ItemStack hand = p.getItemInHand();
					if (hand.getType().getMaxDurability() == 0) {
						final PlayerInventory inv = p.getInventory();
						final ItemStack head = inv.getHelmet();
						inv.removeItem(hand);
						inv.setHelmet(hand);
						inv.setItemInHand(head);
						p.sendMessage(ChatColor.LIGHT_PURPLE + "Enjoy your new hat!");
					} else {
						p.sendMessage(ChatColor.RED + "You cannot use this item as a hat!");
					}
				} else {
					p.sendMessage(ChatColor.RED + "You must have something to wear in your hand.");
				}
			}
			return true;
		}
		return false;
	}
}
