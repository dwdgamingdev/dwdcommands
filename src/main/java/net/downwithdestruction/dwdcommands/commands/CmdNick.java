package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.configuration.PlayerConfig;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdNick implements CommandExecutor {
	private void removeNick(Player t, Player p) {
		PlayerConfig pc = new PlayerConfig(t);
		pc.set(t.getName().trim(), "nickname");
		if (t.isOnline()) {
			t.setDisplayName(t.getName().trim());
			t.setPlayerListName(t.getName().trim());
			t.sendMessage(ChatColor.LIGHT_PURPLE + "Your nickname has been removed.");
		}
		if (p != null) {
			p.sendMessage(ChatColor.LIGHT_PURPLE + "Nickname removed.");
		}
	}
	private void setNick(Player t, Player p, String nick) {
		String prefix = Utils.colorize(DwDCommands.getInstance().nickPrefix);
		if (Utils.isAuthorized(t, "dwdcommands.nick.color")) {
			nick = Utils.colorize(nick);
		} else {
			nick = nick.replaceAll("(&([a-f0-9k-or]))", "");
		}
		PlayerConfig pc = new PlayerConfig(t);
		pc.set(nick, "nickname");
		String newnick = prefix + nick;
		if (t.isOnline()) {
			t.setDisplayName(newnick);
			if (newnick.length() <= 16) t.setPlayerListName(newnick);
			else t.setPlayerListName(newnick.substring(0, 16));
			t.sendMessage(ChatColor.LIGHT_PURPLE + "Your nickname has been set.");
		}
		if (p != null) {
			p.sendMessage(ChatColor.LIGHT_PURPLE + "Nickname set.");
		}
	}
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("nick")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.nick")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			if (args.length == 1) {
				if (args[0].equalsIgnoreCase("off")) {
					removeNick((Player) cs, null);
				} else {
					setNick((Player) cs, null, args[0]);
				}
			} else if (args.length == 2) {
				if (!Utils.isAuthorized(cs, "dwdcommands.others.nick")) {
					Utils.dispNoPerms(cs);
					return true;
				}
				PlayerConfig pc = new PlayerConfig(args[0]);
				if (pc.getString("name") == null) {
					cs.sendMessage(ChatColor.LIGHT_PURPLE + "Player does not exist.");
					return true;
				}
				OfflinePlayer t = DwDCommands.getInstance().getServer().getOfflinePlayer(args[0]);
				if (args[1].equalsIgnoreCase("off")) {
					removeNick((Player) t, (Player) cs);
				} else {
					setNick((Player) t, (Player) cs, args[1]);
				}
			} else {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			return true;
		}
		return false;
	}
}
