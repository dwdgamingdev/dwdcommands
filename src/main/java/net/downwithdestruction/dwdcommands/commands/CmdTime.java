package net.downwithdestruction.dwdcommands.commands;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

public class CmdTime implements CommandExecutor {
	private static final DateFormat STANDARD = new SimpleDateFormat("hh:mma");
	private static final DateFormat MILITARY = new SimpleDateFormat("HH:mm");
	private static boolean formatUsed = false;

	public static Long getValidTime(String time) {
		Long vtime = null;
		try {
			vtime = Long.valueOf(time);
		} catch (Exception e) {
			try { // check if standard time
				if (STANDARD.parse(time) != null) {
					time = MILITARY.format(STANDARD.parse(time)); // It's standard.. Convert to military time.
					formatUsed = true;
				}
			} catch (ParseException pe1) {
				try { // check if military time
					MILITARY.parse(time);
					formatUsed = true;
				} catch (ParseException pe2) {
					return null; // Cant parse time! Not military or standard
				}
			}
			return getTimeTicks(time); // Convert time string to ticks.
		}
		formatUsed = false;
		return vtime; // Converted tick string to ticks.
	}
	
	public static String getTimeString(long time) {
		int hours = (int) ((time / 1000 + 6) % 24);
		int minutes = (int) (60 * (time % 1000) / 1000);
		return String.format("%02d:%02d (%d:%02d %s)", hours, minutes, (hours % 12) == 0 ? 12 : hours % 12, minutes, hours < 12 ? "am" : "pm");
	}
	
	public static long getTimeTicks(String time) {
		int hours, minutes;
		String[] t = time.split(":");
		try {
			hours = (Integer.parseInt(t[0]) - 6) * 1000;
			minutes = Integer.parseInt(t[1]) * 1000 / 60;
		} catch (Exception e) {
			return -1;
		}
		return (hours + minutes);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("time")) {
			// Ignore command from console
			if (sender instanceof ConsoleCommandSender) {
				sender.sendMessage(ChatColor.RED + "This is an in-game only command!");
				return false;
			}
			// View the Time
			if ((args.length < 1) || (args[0].equals("-t"))) {
				if (!Utils.isAuthorized(sender, "dwdcommands.time")) {
					Utils.dispNoPerms(sender);
					return true;
				}
				long ticks = ((Player) sender).getWorld().getTime();
				String time = getTimeString(ticks);
				// Include ticks?
				if ((args.length > 0) && (args[0].equals("-t"))) {
					if (args.length > 1) {
						sender.sendMessage(ChatColor.RED + "-t flag is to be used alone!");
						return false;
					}
					time += " (" + String.valueOf(ticks) + " ticks)";
				}
				// Notify sender
				sender.sendMessage(ChatColor.LIGHT_PURPLE + "" + time);
				return true;
			}
			// Lets set the time
			if (!Utils.isAuthorized(sender, "dwdcommands.time.set")) {
				Utils.dispNoPerms(sender);
				return true;
			}
			// Make sure there's only up to 2 arguments given
			if (args.length > 2) {
				sender.sendMessage(ChatColor.RED + "Max of 2 arguments please!");
				sender.sendMessage(ChatColor.RED + "Please pick only ticks, standard, OR military time!");
				return false;
			}
			// For which world?
			World world = (args.length > 1) ? DwDCommands.getInstance().getServer().getWorld(args[1]) : ((Player) sender).getWorld();
			if (world == null) world = ((Player) sender).getWorld();
			// Is this a valid time (ticks/military/traditional)?
			if (getValidTime(args[0]) == null) {
				sender.sendMessage(ChatColor.RED + "Invalid time specified!");
				return true;
			}
			long time = getValidTime(args[0]);
			if (time < 0) {
				sender.sendMessage(ChatColor.RED + "The time entered was invalid!");
				return true;
			}
			// Actually set the time now
			world.setTime(time);
			// Notify sender in the correct format used to set.
			sender.sendMessage(ChatColor.LIGHT_PURPLE + "Set time in " + ChatColor.GRAY + world.getName() + ChatColor.LIGHT_PURPLE + " to " + ChatColor.GRAY + ((formatUsed) ? getTimeString(time) : time + " ticks") + ChatColor.LIGHT_PURPLE + ".");
			return true;
		}
		return false;
	}
}
