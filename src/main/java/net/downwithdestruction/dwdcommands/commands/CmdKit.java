package net.downwithdestruction.dwdcommands.commands;

import java.util.HashMap;
import java.util.Map;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.configuration.MainConfig;
import net.downwithdestruction.dwdcommands.utils.Logger;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CmdKit implements CommandExecutor {
	private final static Logger logger = Logger.getInstance();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("kit")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.kit")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			boolean kitPerms = DwDCommands.getInstance().kitPerms;
			if (args.length < 1) {
				final Map<String, Object> opts = DwDCommands.getInstance().getConfig().getConfigurationSection("kits").getValues(false);
				if (opts.keySet().isEmpty()) {
					cs.sendMessage(ChatColor.RED + "No kits found!");
					return true;
				}
				String kits = "";
				for (String s : opts.keySet()) {
					if (kitPerms && Utils.isAuthorized(cs, "dwdcommands.kit." + s)) {
						kits = (kits.isEmpty()) ? kits + s : kits + ", " + s;
					} else if (!kitPerms) {
						kits = (kits.isEmpty()) ? kits + s : kits + ", " + s;
					}
				}
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Kits:");
				if (kits.isEmpty()) return true;
				cs.sendMessage(kits);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) cs;
			MainConfig conf = new MainConfig();
			if (conf.get("kits") == null) {
				cs.sendMessage(ChatColor.RED + "No kits defined!");
				return true;
			}
			String kitname = args[0].trim();
			if (conf.get("kits." + kitname) == null) {
				cs.sendMessage(ChatColor.RED + "That kit does not exist!");
				return true;
			}
			java.util.List<String> kits = conf.getStringList("kits." + kitname + ".items");
			if (kits == null) {
				cs.sendMessage(ChatColor.RED + "That kit does not exist!");
				return true;
			}
			if (kitPerms && !Utils.isAuthorized(cs, "dwdcommands.kit." + kitname)) {
				cs.sendMessage(ChatColor.RED + "You don't have permission for that kit!");
				logger.warn(cs.getName() + " was denied access to the command!");
				return true;
			}
			if (Utils.isTimeStampValid(p, "kits." + kitname + ".cooldown") && !Utils.isAuthorized(cs, "dwdcommands.exempt.cooldown.kits")) {
				long ts = Utils.getTimeStamp(p, "kits." + kitname + ".cooldown");
				if (ts > 0) {
					p.sendMessage(ChatColor.RED + "You can't use that kit for" + ChatColor.GRAY + Utils.formatDateDiff(ts) + ChatColor.RED + ".");
					return true;
				}
			}
			if (conf.get("kits." + kitname + ".cooldown") != null) {
				long cd = conf.getLong("kits." + kitname + ".cooldown");
				Utils.setTimeStamp(p, cd, "kits." + kitname + ".cooldown");
			}
			if (kits.size() < 1) {
				cs.sendMessage(ChatColor.RED + "That kit was configured wrong!");
				return true;
			}
			for (String s : kits) {
				String[] kit = s.trim().split(":");
				if (kit.length < 2) {
					cs.sendMessage(ChatColor.RED + "That kit was configured wrong!");
					return true;
				}
				int id;
				int amount;
				int data = -1;
				try {
					id = Integer.parseInt(kit[0]);
				} catch (Exception e) {
					cs.sendMessage(ChatColor.RED + "That kit was configured wrong!");
					return true;
				}
				try {
					amount = Integer.parseInt(kit[1]);
				} catch (Exception e) {
					cs.sendMessage(ChatColor.RED + "That kit was configured wrong!");
					return true;
				}
				try {
					data = Integer.parseInt(kit[2]);
				} catch (Exception ignored) {
				}
				if (id < 1 || amount < 1) {
					cs.sendMessage(ChatColor.RED + "That kit was configured wrong!");
					return true;
				}
				if (Material.getMaterial(id) == null) {
					cs.sendMessage(ChatColor.RED + "Invalid item ID in kit: " + ChatColor.GRAY + id);
					return true;
				}
				ItemStack item;
				if (data > -1) {
					item = new ItemStack(id, amount, (short) data);
				} else {
					item = new ItemStack(id, amount);
				}
				HashMap<Integer, ItemStack> left = p.getInventory().addItem(item);
				if (!left.isEmpty()) {
					for (ItemStack is : left.values()) {
						p.getWorld().dropItemNaturally(p.getLocation(), is);
					}
				}
			}
			p.sendMessage(ChatColor.LIGHT_PURPLE + "Giving you the kit \"" + ChatColor.GRAY + kitname + ChatColor.LIGHT_PURPLE + ".\"");
			return true;
		}
		return false;
	}
}
