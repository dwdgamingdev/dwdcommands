package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

public class CmdFly implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("fly")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.fly")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player) & args.length < 1) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			if (args.length < 1) {
				Player p = (Player) cs;
				if (p.getAllowFlight()) p.setAllowFlight(false);
				else p.setAllowFlight(true);
				String status = (p.getAllowFlight()) ? "on" : "off";
				p.sendMessage(ChatColor.LIGHT_PURPLE + "Toggled flight to " + ChatColor.GRAY + status + ChatColor.LIGHT_PURPLE + ".");
			} else {
				if (!Utils.isAuthorized(cs, "dwdcommands.others.fly")) {
					Utils.dispNoPerms(cs);
					return true;
				}
				Player t = DwDCommands.getInstance().getServer().getPlayer(args[0]);
				if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
					cs.sendMessage(ChatColor.RED + "That player does not exist!");
					return true;
				}
				if (t.getAllowFlight()) t.setAllowFlight(false);
				else t.setAllowFlight(true);
				String status = (t.getAllowFlight()) ? "on" : "off";
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Toggled flight to " + ChatColor.GRAY + status + ChatColor.LIGHT_PURPLE + " on " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
				t.sendMessage(ChatColor.LIGHT_PURPLE + "You have had flight toggled to " + ChatColor.GRAY + status + ChatColor.LIGHT_PURPLE + ".");
			}
			return true;
		}
		return false;
	}
}
