package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.configuration.SpawnConfig;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdSetSpawn implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("setspawn")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.setspawn")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) cs;
			SpawnConfig sc = new SpawnConfig();
			float pitch = p.getLocation().getPitch();
			float yaw = p.getLocation().getYaw();
			double x = p.getLocation().getX();
			double y = p.getLocation().getY();
			double z = p.getLocation().getZ();
			String world = p.getWorld().getName();
			p.getWorld().setSpawnLocation((int) x, (int) y, (int) z);
			sc.setFloat(pitch, "spawns." + world + ".pitch");
			sc.setFloat(yaw, "spawns." + world + ".yaw");
			sc.setDouble(x, "spawns." + world + ".x");
			sc.setDouble(y, "spawns." + world + ".y");
			sc.setDouble(z, "spawns." + world + ".z");
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "The spawn point of " + ChatColor.GRAY + "[" + ChatColor.GOLD + world + ChatColor.GRAY + "]" + ChatColor.LIGHT_PURPLE + " is set.");
			return true;
		}
		return false;
	}
}
