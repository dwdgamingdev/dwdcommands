package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.configuration.PlayerConfig;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdSeen implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("seen")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.seen")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			OfflinePlayer t = DwDCommands.getInstance().getServer().getOfflinePlayer(args[0]);
			if (t.isOnline()) {
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "The player " + ChatColor.GRAY + ((Player) t).getDisplayName() + ChatColor.LIGHT_PURPLE + " was last seen " + ChatColor.GRAY + "now" + ChatColor.LIGHT_PURPLE + ".");
				return true;
			}
			PlayerConfig pc = new PlayerConfig(t);
			if (!pc.exists()) {
				cs.sendMessage(ChatColor.RED + "That player doesn't exist!");
				return true;
			}
			if (pc.get("seen") == null) {
				cs.sendMessage(ChatColor.RED + "I don't know when that player was last seen!");
				return true;
			}
			long seen = pc.getLong("seen");
			String lastseen = Utils.formatDateDiff(seen);
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "The player " + ChatColor.GRAY + t.getName() + ChatColor.LIGHT_PURPLE + " was last seen" + ChatColor.GRAY + lastseen + ChatColor.LIGHT_PURPLE + " ago.");
			return true;
		}
		return false;
	}
}
