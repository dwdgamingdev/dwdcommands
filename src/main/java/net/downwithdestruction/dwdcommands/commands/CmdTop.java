package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

public class CmdTop implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("top")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.top")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) cs;
			final int topX = p.getLocation().getBlockX();
			final int topZ = p.getLocation().getBlockZ();
			final int topY = p.getWorld().getHighestBlockYAt(topX, topZ);
			p.teleport(new Location(p.getWorld(), p.getLocation().getX(), topY + 1, p.getLocation().getZ(), p.getLocation().getYaw(), p.getLocation().getPitch()), TeleportCause.COMMAND);
			p.sendMessage("");
			return true;
		}
		return false;
	}
}
