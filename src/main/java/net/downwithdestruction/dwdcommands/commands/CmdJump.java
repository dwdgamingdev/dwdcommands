package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdJump implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("jump")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.jump")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) cs;
			Block bb = Utils.getTarget(p);
			if (bb == null) {
				cs.sendMessage(ChatColor.RED + "Can't jump there!");
				return true;
			}
			Location bLoc = new Location(p.getWorld(), bb.getLocation().getX() + .5, bb.getLocation().getY() + 1, bb.getLocation().getZ() + .5, p.getLocation().getYaw(), p.getLocation().getPitch());
			p.teleport(bLoc);
			return true;
		}
		return false;
	}
}
