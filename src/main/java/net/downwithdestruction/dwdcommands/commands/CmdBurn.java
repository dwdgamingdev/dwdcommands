package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdBurn implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("burn")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.burn")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 1) {
				cmd.getDescription();
				return false;
			}
			Player t = DwDCommands.getInstance().getServer().getPlayer(args[0].trim());
			if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
				cs.sendMessage(ChatColor.RED + "That player is not online.");
				return true;
			}
			int time;
			if (args.length > 1) {
				try {
					time = Integer.parseInt(args[1]);
				} catch(NumberFormatException e) {
					cmd.getDescription();
					cs.sendMessage(ChatColor.RED + "Damage must be a number.");
					return false;
				}
			} else {
				time = 5; // default time (in seconds) if not specified
			}
			if (Utils.isAuthorized(t, "dwdcommands.exempt.burn")) {
				cs.sendMessage(ChatColor.RED + "You cannot burn that player.");
				return true;
			}
			t.setFireTicks(time * 20);
			String name = (cs instanceof Player) ? ((Player) cs).getDisplayName() : "CONSOLE";
			t.sendMessage(ChatColor.LIGHT_PURPLE + "You have just been burned by " + ChatColor.GRAY + name + ChatColor.LIGHT_PURPLE + ".");
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "You just burned " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
			return true;
		}
		return false;
	}
}
