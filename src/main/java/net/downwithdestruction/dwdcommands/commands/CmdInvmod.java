package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

public class CmdInvmod implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("invmod")) {
			if (!Utils.isAuthorized(sender, "dwdcommands.invmod")) {
				Utils.dispNoPerms(sender);
				return true;
			}
			if (args.length < 1) {
				sender.sendMessage(cmd.getDescription());
				return false;
			}
			if (!(sender instanceof Player)) {
				sender.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) sender;
			Player t = DwDCommands.getInstance().getServer().getPlayer(args[0]);
			if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
				sender.sendMessage(ChatColor.RED + "That player doesn't exist!");
				return true;
			}
			if (Utils.isAuthorized(t, "dwdcommands.exempt.invmod")) {
				sender.sendMessage(ChatColor.RED + "You cannot modify that player's inventory.");
				return true;
			}
			p.openInventory(t.getInventory());
			String possessive = (t.getName().toLowerCase().endsWith("s")) ? "'" : "'s";
			sender.sendMessage(ChatColor.LIGHT_PURPLE + "Opened " + ChatColor.GRAY + t.getDisplayName() + possessive + ChatColor.LIGHT_PURPLE + " inventory.");
			return true;
		}
		return false;
	}
}
