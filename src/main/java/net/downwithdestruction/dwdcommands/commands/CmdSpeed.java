package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdSpeed implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("speed")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.speed")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			Player p = (Player) cs;
			float flySpeed;
			try {
				flySpeed = Float.valueOf(args[0]);
			} catch (NumberFormatException e) {
				cs.sendMessage(ChatColor.RED + "Please enter a valid number!");
				return true;
			}
			if (flySpeed < 0 || flySpeed > 10) {
				cs.sendMessage(ChatColor.RED + "Speed must be between 0.0 and 10.0!");
				return true;
			}
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "Set your fly speed to " + ChatColor.GRAY + flySpeed + ChatColor.LIGHT_PURPLE + ".");
			p.setFlySpeed(flySpeed / 10);
			return true;
		}
		return false;
	}
}
