package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdDepth implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("depth")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.depth")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) cs;
			final int depth = p.getPlayer().getLocation().getBlockY() - 63;
			if (depth > 0) {
				p.sendMessage(ChatColor.LIGHT_PURPLE + "You are " + depth + " block" + ((depth>1) ? "s" : "") + " above sea level.");
			} else if (depth < 0) {
				p.sendMessage(ChatColor.LIGHT_PURPLE + "You are" + -depth + " block" + ((depth<-1) ? "s" : "") + " below sea level.");
			} else {
				p.sendMessage(ChatColor.LIGHT_PURPLE + "You are at sea level.");
			}
			return true;
		}
		return false;
	}
}
