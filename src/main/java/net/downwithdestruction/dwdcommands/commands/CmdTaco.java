package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Cow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class CmdTaco implements CommandExecutor {
	Player p = null;
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("taco")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.taco")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			p = (Player) cs;
			p.setSneaking(true);
			Location loc = p.getLocation();
			loc.getWorld().createExplosion(loc, 0F);
			Cow cow = (Cow)p.getWorld().spawn(loc, EntityType.COW.getEntityClass());
			if (cow == null) { return true; }
			cow.setVelocity(p.getEyeLocation().getDirection().multiply(-2));
			DwDCommands.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(DwDCommands.getInstance(), new Runnable() {
				@Override
				public void run() {
					p.setSneaking(false);
				}
			}, 20);
			return true;
		}
		return false;
	}
}
