package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.configuration.SpawnConfig;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdSpawn implements CommandExecutor {
	public static Location getWorldSpawn(World world) {
		SpawnConfig sc = new SpawnConfig();
		String w = world.getName();
		Double x = sc.getDouble("spawns." + w + ".x");
		Double y = sc.getDouble("spawns." + w + ".y");
		Double z = sc.getDouble("spawns." + w + ".z");
		Float yaw = sc.getFloat("spawns." + w + ".yaw");
		Float pitch = sc.getFloat("spawns." + w + ".pitch");
		Location l;
		try {
			l = new Location(world, x, y, z, yaw, pitch);
		} catch (Exception e) {
			l = world.getSpawnLocation();
		}
		return l;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("spawn")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.spawn")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) cs;
			World world;
			if (args.length > 0) {
				if (!Utils.isAuthorized(cs, "dwdcommands.spawn.changeworld")) {
					Utils.dispNoPerms(cs);
					return true;
				}
				world = DwDCommands.getInstance().getServer().getWorld(args[0]);
				if (world == null) {
					cs.sendMessage(ChatColor.RED + "No such world!");
					return true;
				}
			} else world = p.getWorld();
			p.sendMessage(ChatColor.LIGHT_PURPLE + "Going to spawn in " + ChatColor.GRAY + "[" + ChatColor.GOLD + world.getName() + ChatColor.GRAY + "]" + ChatColor.LIGHT_PURPLE + ".");
			Utils.teleport(p, getWorldSpawn(world));
			return true;
		}
		return false;
	}
}
