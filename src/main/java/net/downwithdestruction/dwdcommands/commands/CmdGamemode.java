package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Logger;
import net.downwithdestruction.dwdcommands.utils.Utils;

public class CmdGamemode implements CommandExecutor {
	private final Logger logger = Logger.getInstance();
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("gamemode")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.gamemode")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player) && args.length < 1) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			if (args.length < 1) {
				if (!(cs instanceof Player)) {
					cs.sendMessage(cmd.getDescription());
					return false;
				}
				Player p = (Player) cs;
				GameMode toSet = (p.getGameMode().equals(GameMode.CREATIVE)) ? GameMode.SURVIVAL : GameMode.CREATIVE;
				p.setGameMode(toSet);
				logger.info(p.getName() + "'s gamemode has been set to " + p.getGameMode().name());
				p.sendMessage(ChatColor.LIGHT_PURPLE + "Your game mode has been set to " + ChatColor.GRAY + toSet.name().toLowerCase() + ChatColor.LIGHT_PURPLE + ".");
				return true;
			}
			if (args.length > 0) {
				Player t = DwDCommands.getInstance().getServer().getPlayer(args[0].trim());
				if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
					cs.sendMessage(ChatColor.RED + "That player does not exist!");
					return true;
				}
				if (!Utils.isAuthorized(cs, "dwdcommands.others.gamemode")) {
					cs.sendMessage(ChatColor.RED + "You can't change other players' gamemodes!");
					return true;
				}
				if (Utils.isAuthorized(t, "dwdcommands.exempt.gamemode")) {
					cs.sendMessage(ChatColor.RED + "You cannot change that player's gamemode.");
					return true;
				}
				GameMode toSet = (t.getGameMode().equals(GameMode.CREATIVE)) ? GameMode.SURVIVAL : GameMode.CREATIVE;
				t.setGameMode(toSet);
				logger.info(t.getName() + "'s gamemode has been set to " + t.getGameMode().name());
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "You have changed " + ChatColor.GRAY + t.getDisplayName() + "\'s" + ChatColor.LIGHT_PURPLE + " game mode to " + ChatColor.GRAY + toSet.name().toLowerCase() + ChatColor.LIGHT_PURPLE + ".");
				t.sendMessage(ChatColor.LIGHT_PURPLE + "Your game mode has been changed to " + ChatColor.GRAY + toSet.name().toLowerCase() + ChatColor.LIGHT_PURPLE + ".");
				return true;
			}
		}
		return false;
	}
}
