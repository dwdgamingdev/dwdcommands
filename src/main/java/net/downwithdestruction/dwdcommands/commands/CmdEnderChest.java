package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdEnderChest implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("enderchest")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.enderchest")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) cs;
			if (args.length > 0) {
				if (!Utils.isAuthorized(cs, "dwdcommands.others.enderchest")) {
					Utils.dispNoPerms(cs);
					return true;
				}
				Player t = DwDCommands.getInstance().getServer().getPlayer(args[0]);
				if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
					p.sendMessage(ChatColor.RED + "That player is not online!");
					return true;
				}
				p.openInventory(t.getEnderChest());
				p.sendMessage(ChatColor.LIGHT_PURPLE + "Opened " + t.getDisplayName() + "'s enderchest.");
				return true;
			}
			p.openInventory(p.getEnderChest());
			p.sendMessage(ChatColor.LIGHT_PURPLE + "Opened your enderchest.");
			return true;
		}
		return false;
	}
}
