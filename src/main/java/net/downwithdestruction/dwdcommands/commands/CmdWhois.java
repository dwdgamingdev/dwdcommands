package net.downwithdestruction.dwdcommands.commands;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Date;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.configuration.PlayerConfig;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdWhois implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("whois")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.whois")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			OfflinePlayer t = DwDCommands.getInstance().getServer().getOfflinePlayer(args[0]);
			File file = new File(DwDCommands.getInstance().getDataFolder() + File.separator + "userdata" + File.separator + args[0].toLowerCase() + ".yml");
			if (!file.exists()) {
				cs.sendMessage(ChatColor.RED + "That player has never played before!");
				return true;
			}
			file = null;
			PlayerConfig pc = new PlayerConfig(t);
			DecimalFormat df = new DecimalFormat("#.##");
			String ip = pc.getString("ipAddress");
			String name = pc.getString("name");
			String nickname = pc.getString("nickname");
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "=====================");
			cs.sendMessage(ChatColor.LIGHT_PURPLE + ((t.isOnline()) ? "Whois" : "Whowas") + " for " + ChatColor.GRAY + name);
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "Nickname: " + nickname);
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "IP: " + ChatColor.GRAY + ip);
			long timestamp = Utils.getTimeStamp(t, "seen");
			String lastseen = (timestamp < 0) ? "unknown" : Utils.formatDateDiff(timestamp);
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "Last seen:" + ChatColor.GRAY + ((t.isOnline()) ? " now" : lastseen));
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "First played:" + ChatColor.GRAY + Utils.formatDateDiff(t.getFirstPlayed()));
			if (t.isOnline()) {
				Player p = (Player) t;
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Gamemode: " + ChatColor.GRAY + p.getGameMode().name().toLowerCase());
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Can fly: " + ChatColor.GRAY + (p.getAllowFlight() ? "Yes" : "No"));
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Health/Hunger/Saturation: " + ChatColor.GRAY + p.getHealth() / 2 + ChatColor.LIGHT_PURPLE + "/" + ChatColor.GRAY + p.getFoodLevel() / 2 + ChatColor.LIGHT_PURPLE + "/" + ChatColor.GRAY + p.getSaturation() / 2);
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Total Exp/Exp %/Level: " + ChatColor.GRAY + p.getTotalExperience() + ChatColor.LIGHT_PURPLE + "/" + ChatColor.GRAY + df.format(p.getExp() * 100) + "%" + ChatColor.LIGHT_PURPLE + "/" + ChatColor.GRAY + p.getLevel());
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Item in hand: " + ChatColor.GRAY + Utils.getItemName(p.getItemInHand()));
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Alive for:" + ChatColor.GRAY + Utils.formatDateDiff(new Date().getTime() - p.getTicksLived() * 50));
				Location l = p.getLocation();
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Last position: " + "(" + ChatColor.GRAY + (int) l.getX() + ChatColor.LIGHT_PURPLE + ", " + ChatColor.GRAY + (int) l.getY() + ChatColor.LIGHT_PURPLE + ", " + ChatColor.GRAY + (int) l.getZ() + ChatColor.LIGHT_PURPLE + ")");
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Last world: " + ChatColor.GRAY + l.getWorld().getName());
			} else {
				World w = DwDCommands.getInstance().getServer().getWorld(pc.getString("lastlocation.world"));
				if (w != null) {
					Location l = new Location(w, pc.getDouble("lastlocation.x"), pc.getDouble("lastlocation.y"), pc.getDouble("lastlocation.z"));
					cs.sendMessage(ChatColor.LIGHT_PURPLE + "Last position: " + "(" + ChatColor.GRAY + (int) l.getX() + ChatColor.LIGHT_PURPLE + ", " + ChatColor.GRAY + (int) l.getY() + ChatColor.LIGHT_PURPLE + ", " + ChatColor.GRAY + (int) l.getZ() + ChatColor.LIGHT_PURPLE + ")");
					cs.sendMessage(ChatColor.LIGHT_PURPLE + "Last world: " + ChatColor.GRAY + l.getWorld().getName());
				}
			}
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "=====================");
			return true;
		}
		return false;
	}
}
