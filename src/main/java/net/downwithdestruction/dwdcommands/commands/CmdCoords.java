package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdCoords implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("coords")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.coords")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) cs;
			if (args.length < 1) {
				Location l = p.getLocation();
				p.sendMessage(ChatColor.LIGHT_PURPLE + "Your location:");
				p.sendMessage(ChatColor.LIGHT_PURPLE + "x: " + ChatColor.GRAY + l.getX());
				p.sendMessage(ChatColor.LIGHT_PURPLE + "y: " + ChatColor.GRAY + l.getY());
				p.sendMessage(ChatColor.LIGHT_PURPLE + "z: " + ChatColor.GRAY + l.getZ());
				p.sendMessage(ChatColor.LIGHT_PURPLE + "pitch: " + ChatColor.GRAY + l.getPitch());
				p.sendMessage(ChatColor.LIGHT_PURPLE + "yaw: " + ChatColor.GRAY + l.getYaw());
				p.sendMessage(ChatColor.LIGHT_PURPLE + "world: " + ChatColor.GRAY + l.getWorld().getName());
				return true;
			}
			if (!Utils.isAuthorized(cs, "dwdcommands.others.coords")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			Player t = DwDCommands.getInstance().getServer().getPlayer(args[0]);
			if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
				cs.sendMessage(ChatColor.RED + "That player does not exist!");
				return true;
			}
			Location l = t.getLocation();
			p.sendMessage(ChatColor.LIGHT_PURPLE + t.getDisplayName() + "'s location:");
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "x: " + ChatColor.GRAY + l.getX());
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "y: " + ChatColor.GRAY + l.getY());
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "z: " + ChatColor.GRAY + l.getZ());
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "pitch: " + ChatColor.GRAY + l.getPitch());
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "yaw: " + ChatColor.GRAY + l.getYaw());
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "world: " + ChatColor.GRAY + l.getWorld().getName());
			return true;
		}
		return false;
	}
}
