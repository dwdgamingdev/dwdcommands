package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;

public class CmdSudo implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("sudo")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.sudo")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 2) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			Player t = DwDCommands.getInstance().getServer().getPlayer(args[0]);
			if ((t == null) || (DwDCommands.getInstance().isVanished(t, cs))) {
				cs.sendMessage(ChatColor.RED + "That player does not exist!");
				return true;
			}
			if (Utils.isAuthorized(t, "dwdcommands.exempt.sudo")) {
				cs.sendMessage(ChatColor.RED + "You cannot use /sudo on that player!");
				return true;
			}
			String command = args[1];
			String[] arguments = new String[args.length - 2];
			if (arguments.length > 0) {
				System.arraycopy(args, 2, arguments, 0, args.length - 2);
			}
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "Making " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + " execute command: " + ChatColor.GRAY + "/" + Utils.getFinalArg(args, 1));
			PluginCommand execCommand = DwDCommands.getInstance().getServer().getPluginCommand(command);
			if (execCommand != null) {
				execCommand.execute(t, command, arguments);
			}
			return true;
		}
		return false;
	}
}
