package net.downwithdestruction.dwdcommands.commands;

import java.util.HashMap;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.block.Furnace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.FurnaceInventory;

public class CmdFurnace implements CommandExecutor {
	public HashMap<Player, Furnace> furnacedb = new HashMap<Player, Furnace>();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("furnace")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.furnace")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			if ((args.length < 1) || (args.length > 1)) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			Player p = (Player) cs;
			String command = args[0].toLowerCase();
			if (command.equals("set")) {
				if (!(Utils.getTarget(p).getState() instanceof Furnace)) {
					cs.sendMessage(ChatColor.RED + "That's not a furnace!");
					return true;
				}
				Furnace f = (Furnace) Utils.getTarget(p).getState();
				if (DwDCommands.getInstance().hasLWC) {
					if (!DwDCommands.getInstance().lwc.canAccessProtection(p, Utils.getTarget(p))) {
						cs.sendMessage(ChatColor.RED + "You do not have access to this furnace.");
						return true;
					}
				}
				furnacedb.put(p, f);
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Furnace set.");
			} else if (command.equals("show")) {
				if (!furnacedb.containsKey(p)) {
					cs.sendMessage(ChatColor.RED + "You must first set a furnace!");
					return true;
				}
				Furnace f = furnacedb.get(p);
				if (!(f.getBlock().getState() instanceof Furnace)) {
					cs.sendMessage(ChatColor.RED + "The furnace is no longer there!");
					return true;
				}
				if (DwDCommands.getInstance().hasLWC) {
					if (!DwDCommands.getInstance().lwc.canAccessProtection(p, f.getBlock())) {
						cs.sendMessage(ChatColor.RED + "You do not have access to this furnace.");
						return true;
					}
				}
				f = (Furnace) f.getBlock().getState();
				FurnaceInventory fi = f.getInventory();
				p.openInventory(fi);
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Opened your furnace for you.");
			} else {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			return true;
		}
		return false;
	}
}
