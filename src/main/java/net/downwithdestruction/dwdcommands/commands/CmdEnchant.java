package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.downwithdestruction.dwdcommands.utils.Utils;

public class CmdEnchant implements CommandExecutor {
	public void sendEnchantmentList(CommandSender cs) {
		StringBuilder sb = new StringBuilder();
		for (Enchantment e : Enchantment.values()) {
			sb.append(ChatColor.GRAY);
			sb.append(e.getName());
			sb.append(ChatColor.RESET);
			sb.append(", ");
		}
		cs.sendMessage(sb.substring(0, sb.length() - 4)); // "&r, " = 4
	}

	public boolean onCommand(CommandSender cs, Command cmd, String label, String args[]) {
		if (cmd.getName().equalsIgnoreCase("enchant")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.enchant")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			if (args.length < 1) {
				sendEnchantmentList(cs);
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			Player p = (Player) cs;
			ItemStack hand = p.getItemInHand();
			if (hand == null || hand.getType() == Material.AIR) {
				cs.sendMessage(ChatColor.RED + "Air cannot be enchanted!");
				return true;
			}
			Enchantment toAdd = Enchantment.getByName(args[0].toUpperCase());
			int MAX_LEVEL = -1;
			int MAX_INT = -2;
			int REMOVE = 0;
			if (toAdd == null) {
				if (args[0].equalsIgnoreCase("all")) {
					int level;
					if (args.length < 2) level = -1;
					else if (args.length > 1 && args[1].equalsIgnoreCase("max")) level = -2;
					else {
						try {
							level = Integer.parseInt(args[1]);
						} catch (NumberFormatException e) {
							cs.sendMessage(ChatColor.RED + "The level supplied was not a number!");
							return true;
						}
						if (level < 0) {
							cs.sendMessage(ChatColor.RED + "The level cannot be below zero!");
							return true;
						}
						if (level > 30) {
							cs.sendMessage(ChatColor.RED + "The level cannot be above " + 30 + "!");
							return true;
						}
					}
					if (level == REMOVE) {
						for (Enchantment e : Enchantment.values()) {
							if (!hand.containsEnchantment(e)) continue;
							hand.removeEnchantment(e);
						}
						cs.sendMessage(ChatColor.LIGHT_PURPLE + "Removed all enchantments from " + ChatColor.GRAY + Utils.getItemName(hand) + ChatColor.LIGHT_PURPLE + ".");
					} else if (level == MAX_LEVEL) {
						for (Enchantment e : Enchantment.values()) hand.addUnsafeEnchantment(e, e.getMaxLevel());
						cs.sendMessage(ChatColor.LIGHT_PURPLE + "Added " + ChatColor.GRAY + "all" + ChatColor.LIGHT_PURPLE + " enchantments to " + ChatColor.GRAY + Utils.getItemName(hand) + ChatColor.LIGHT_PURPLE + " at their maximum levels.");
					} else if (level == MAX_INT) {
						for (Enchantment e : Enchantment.values()) hand.addUnsafeEnchantment(e, 30);
						cs.sendMessage(ChatColor.LIGHT_PURPLE + "Added " + ChatColor.GRAY + "all" + ChatColor.LIGHT_PURPLE + " enchantments to " + ChatColor.GRAY + Utils.getItemName(hand) + ChatColor.LIGHT_PURPLE + " at level " + ChatColor.GRAY + 30 + ChatColor.LIGHT_PURPLE + ".");
					} else {
						for (Enchantment e : Enchantment.values()) hand.addUnsafeEnchantment(e, level);
						cs.sendMessage(ChatColor.LIGHT_PURPLE + "Added " + ChatColor.GRAY + "all" + ChatColor.LIGHT_PURPLE + " enchantments to " + ChatColor.GRAY + Utils.getItemName(hand) + ChatColor.LIGHT_PURPLE + " at level " + ChatColor.GRAY + level + ChatColor.LIGHT_PURPLE + ".");
					}
					return true;
				}
				sendEnchantmentList(cs);
				cs.sendMessage(ChatColor.RED + "No such enchantment!");
				return true;
			}
			int level;
			if (args.length < 2) level = -1;
			else if (args.length > 1 && args[1].equalsIgnoreCase("max")) level = -2;
			else {
				try {
					level = Integer.parseInt(args[1]);
				} catch (NumberFormatException e) {
					cs.sendMessage(ChatColor.RED + "The level supplied was not a number or greater than " + Integer.MAX_VALUE + "!");
					return true;
				}
				if (level < 0) {
					cs.sendMessage(ChatColor.RED + "The level cannot be below zero!");
					return true;
				}
				if (level > 30) {
					cs.sendMessage(ChatColor.RED + "The level cannot be above " + 30 + "!");
					return true;
				}
			}
			if (level == REMOVE) {
				if (!hand.containsEnchantment(toAdd)) {
					cs.sendMessage(ChatColor.RED + "That " + ChatColor.GRAY + Utils.getItemName(hand) + ChatColor.LIGHT_PURPLE + " does not contain " + ChatColor.GRAY + toAdd.getName().toLowerCase().replace("_", " ") + ChatColor.LIGHT_PURPLE + ".");
					return true;
				}
				hand.removeEnchantment(toAdd);
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Added " + ChatColor.GRAY + toAdd.getName().toLowerCase().replace("_", " ") + ChatColor.LIGHT_PURPLE + " from " + ChatColor.GRAY + Utils.getItemName(hand) + ChatColor.LIGHT_PURPLE + ".");
			} else if (level == MAX_LEVEL) {
				hand.addUnsafeEnchantment(toAdd, toAdd.getMaxLevel());
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Added " + ChatColor.GRAY + toAdd.getName().toLowerCase().replace("_", " ") + ChatColor.LIGHT_PURPLE + " to " + ChatColor.GRAY + Utils.getItemName(hand) + ChatColor.LIGHT_PURPLE + " at level " + ChatColor.GRAY + toAdd.getMaxLevel() + ChatColor.LIGHT_PURPLE + ".");
			} else if (level == MAX_INT) {
				hand.addUnsafeEnchantment(toAdd, 30);
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Added " + ChatColor.GRAY + toAdd.getName().toLowerCase().replace("_", " ") + ChatColor.LIGHT_PURPLE + " to " + ChatColor.GRAY + Utils.getItemName(hand) + ChatColor.LIGHT_PURPLE + " at level " + ChatColor.GRAY + 30 + ChatColor.LIGHT_PURPLE + ".");
			} else {
				hand.addUnsafeEnchantment(toAdd, level);
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Added " + ChatColor.GRAY + toAdd.getName().toLowerCase().replace("_", " ") + ChatColor.LIGHT_PURPLE + " to " + ChatColor.GRAY + Utils.getItemName(hand) + ChatColor.LIGHT_PURPLE + " at level " + ChatColor.GRAY + level + ChatColor.LIGHT_PURPLE + ".");
			}
			return true;
		}
		return false;
	}
}
