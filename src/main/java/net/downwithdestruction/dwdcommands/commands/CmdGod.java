package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.downwithdestruction.dwdcommands.configuration.PlayerConfig;
import net.downwithdestruction.dwdcommands.utils.Logger;
import net.downwithdestruction.dwdcommands.utils.Utils;
import net.downwithdestruction.dwdcommands.DwDCommands;

public class CmdGod implements CommandExecutor {
	private final static Logger logger = Logger.getInstance();
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("god")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.god")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 1) {
				if (!(cs instanceof Player)) {
					cs.sendMessage(cmd.getDescription());
					return false;
				}
				Player t = (Player) cs;
				PlayerConfig pc = new PlayerConfig(t);
				t.setHealth(20);
				t.setFoodLevel(20);
				t.setSaturation(20F);
				if (!pc.getBoolean("godmode")) {
					cs.sendMessage(ChatColor.LIGHT_PURPLE + "You have enabled godmode for yourself.");
					pc.setBoolean(true, "godmode");
					return true;
				} else {
					cs.sendMessage(ChatColor.LIGHT_PURPLE + "You have disabled godmode for yourself.");
					pc.setBoolean(false, "godmode");
					return true;
				}
			}
			if (args.length > 0) {
				if (!Utils.isAuthorized(cs, "dwdcommands.others.god")) {
					cs.sendMessage(ChatColor.RED + "You don't have permission for that!");
					logger.warn(cs.getName() + " was denied access to the command!");
					return true;
				}
				Player t = DwDCommands.getInstance().getServer().getPlayer(args[0]);
				if (t != null) {
					PlayerConfig pc = new PlayerConfig(t);
					if (!pc.getBoolean("godmode")) {
						if (!pc.exists()) {
							cs.sendMessage(ChatColor.RED + "That player doesn't exist!");
							return true;
						}
						t.setHealth(20);
						t.setFoodLevel(20);
						t.setSaturation(20F);
						t.sendMessage(ChatColor.LIGHT_PURPLE + "The player " + ChatColor.GRAY + ((Player) cs).getDisplayName() + ChatColor.LIGHT_PURPLE + " has enabled godmode for you!");
						cs.sendMessage(ChatColor.LIGHT_PURPLE + "You have enabled godmode for " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
						pc.setBoolean(true, "godmode");
						return true;
					} else {
						t.sendMessage(ChatColor.RED + "The player " + ChatColor.GRAY + ((Player) cs).getDisplayName() + ChatColor.RED + " has disabled godmode for you!");
					}
					cs.sendMessage(ChatColor.LIGHT_PURPLE + "You have disabled godmode for " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
					pc.setBoolean(false, "godmode");
					return true;
				}
			}
			OfflinePlayer t2 = DwDCommands.getInstance().getServer().getOfflinePlayer(args[0].trim());
			PlayerConfig pc = new PlayerConfig(t2);
			if (!pc.getBoolean("godmode")) {
				if (!pc.exists()) {
					cs.sendMessage(ChatColor.RED + "That player doesn't exist!");
					return true;
				}
				if (t2.isOnline()) {
					((Player) t2).setHealth(20);
					((Player) t2).setFoodLevel(20);
					((Player) t2).setSaturation(20F);
					((Player) t2).sendMessage(ChatColor.LIGHT_PURPLE + "The player " + ChatColor.GRAY + ((Player) cs).getDisplayName() + ChatColor.LIGHT_PURPLE + " has enabled godmode for you!");
				}
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "You have enabled godmode for " + ChatColor.GRAY + ((Player) t2).getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
				pc.setBoolean(true, "godmode");
				return true;
			} else {
				if (t2.isOnline()) {
					((Player) t2).setHealth(20);
					((Player) t2).setFoodLevel(20);
					((Player) t2).setSaturation(20F);
					((Player) t2).sendMessage(ChatColor.RED + "The player " + ChatColor.GRAY + ((Player) cs).getDisplayName() + ChatColor.RED + " has disabled godmode for you!");
				}
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "You have disabled godmode for " + ChatColor.GRAY + ((Player) t2).getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
				pc.setBoolean(false, "godmode");
				return true;
			}
		}
		return false;
	}
}
