package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.downwithdestruction.dwdcommands.utils.Utils;

public class CmdThunder implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("thunder")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.thunder")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(ChatColor.RED + "Not enough arguments.");
				return false;
			}
			Player p = (Player) cs;
			World world = p.getWorld();
			boolean setThunder = args[0].equalsIgnoreCase("true");
			world.setThundering(setThunder ? true : false);
			if (args.length > 1) {
				world.setThunderDuration(Integer.parseInt(args[1]) * 20);
			}
			p.sendMessage(ChatColor.LIGHT_PURPLE + "Thunder " + (setThunder ? "Enabled" : "Disabled"));
			return true;
		}
		return false;
	}
}
