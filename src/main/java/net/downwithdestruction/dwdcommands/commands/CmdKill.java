package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdKill implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("kill")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.kill")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			Player t = DwDCommands.getInstance().getServer().getPlayer(args[0].trim());
			if (t == null || DwDCommands.getInstance().isVanished(t, cs)) {
				cs.sendMessage(ChatColor.RED + "That player does not exist!");
				return true;
			}
			if (Utils.isAuthorized(t, "dwdcommands.exempt.kill")) {
				cs.sendMessage(ChatColor.RED + "You cannot kill that player!");
				return true;
			}
			t.setHealth(0);
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "You have killed " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
			return true;
		}
		return false;
	}
}
