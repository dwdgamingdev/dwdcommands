package net.downwithdestruction.dwdcommands.commands;

import java.util.HashMap;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CmdGive implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("give")) {
			if (!Utils.isAuthorized(sender, "dwdcommands.give")) {
				Utils.dispNoPerms(sender);
				return true;
			}
			if (args.length < 2) {
				sender.sendMessage(cmd.getDescription());
				return false;
			}
			Player t = DwDCommands.getInstance().getServer().getPlayer(args[0]);
			if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
				sender.sendMessage(ChatColor.RED + "That player is not online!");
				return true;
			}
			int amount = 1;
			if (args.length == 3) {
				try {
					amount = Integer.parseInt(args[2]);
				} catch (Exception e) {
					sender.sendMessage(ChatColor.RED + "The amount was not a number!");
					return true;
				}
			}
			if (amount < 1) {
				sender.sendMessage(ChatColor.RED + "Invalid amount! You must specify a positive amount.");
				return true;
			}
			String name = args[1];
			ItemStack toInv = Utils.getItem(name, amount);
			if (toInv == null) {
				sender.sendMessage(ChatColor.RED + "Invalid item name!");
				return true;
			}
			Integer itemid = toInv.getTypeId();
			if (itemid == 0) {
				sender.sendMessage(ChatColor.RED + "You cannot spawn air!");
				return true;
			}
			if (!Utils.isAuthorized(sender, "dwdcommands.allowed.item") && !Utils.isAuthorized(sender, "dwdcommands.allowed.item." + itemid)) {
				sender.sendMessage(ChatColor.RED + "You are not allowed to spawn that item!");
				return true;
			}
			HashMap<Integer, ItemStack> left = t.getInventory().addItem(toInv);
			if (!left.isEmpty()) {
				for (ItemStack item : left.values()) {
					t.getWorld().dropItemNaturally(t.getLocation(), item);
				}
			}
			sender.sendMessage(ChatColor.LIGHT_PURPLE + "Giving " + ChatColor.GRAY + amount + ChatColor.LIGHT_PURPLE + " of " + ChatColor.GRAY + Utils.getItemName(Material.getMaterial(itemid)) + ChatColor.LIGHT_PURPLE + " to " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
			t.sendMessage(ChatColor.LIGHT_PURPLE + "You have been given " + ChatColor.GRAY + amount + ChatColor.LIGHT_PURPLE + " of " + ChatColor.GRAY + Utils.getItemName(Material.getMaterial(itemid)) + ChatColor.LIGHT_PURPLE + ".");
			return true;
		}
		return false;
	}
}
