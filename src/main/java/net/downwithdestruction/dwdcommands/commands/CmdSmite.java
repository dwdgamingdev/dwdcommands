package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

public class CmdSmite implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("smite")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.smite")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			if (args.length < 1) {
				Player p = (Player) cs;
				Block bb = Utils.getTarget(p);
				if (bb == null) {
					cs.sendMessage(ChatColor.RED + "Can't smite there!");
					return true;
				}
				Location bLoc = new Location(p.getWorld(), bb.getLocation().getX() + .5, bb.getLocation().getY() + 1, bb.getLocation().getZ() + .5, p.getLocation().getYaw(), p.getLocation().getPitch());
				bLoc.getWorld().strikeLightning(bLoc);
				return true;
			}
			Player t = DwDCommands.getInstance().getServer().getPlayer(args[0].trim());
			if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
				cs.sendMessage(ChatColor.RED + "That player does not exist!");
				return true;
			}
			if (Utils.isAuthorized(t, "dwdcommands.exempt.smite")) {
				cs.sendMessage(ChatColor.RED + "You can't smite that player!");
				return true;
			}
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "Smiting " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
			t.sendMessage(ChatColor.RED + "You have been smited by " + ChatColor.GRAY + ((Player) cs).getDisplayName() + ChatColor.RED + ".");
			t.getWorld().strikeLightning(t.getLocation());
			return true;
		}
		return false;
	}
}
