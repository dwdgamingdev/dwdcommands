package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdBroadcast implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("broadcast")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.broadcast")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			String message = Utils.colorize(Utils.getFinalArg(args, 0));
			DwDCommands.getInstance().getServer().broadcastMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Broadcast" + ChatColor.GRAY + "] " + ChatColor.GREEN + message);
			return true;
		}
		return false;
	}
}
