package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdRide implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("ride")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.ride")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) cs;
			if (args.length < 1) {
				if (p.getVehicle() == null) {
					p.sendMessage(cmd.getDescription());
					return false;
				}
				p.getVehicle().eject();
				p.sendMessage(ChatColor.LIGHT_PURPLE + "You have ejected.");
			}
			if (args.length > 0) {
				Player t = DwDCommands.getInstance().getServer().getPlayer(args[0].trim());
				if (t == null || DwDCommands.getInstance().isVanished(t, cs)) {
					p.sendMessage(ChatColor.RED + "That player does not exist!");
					return true;
				}
				if (p.equals(t)) {
					cs.sendMessage(ChatColor.RED + "You cannot ride yourself.");
					return true;
				}
				if (Utils.isAuthorized(t, "dwdcommands.exempt.ride")) {
					cs.sendMessage(ChatColor.RED + "You cannot ride that player!");
					return true;
				}
				t.setPassenger(p);
			}
			return true;
		}
		return false;
	}
}
