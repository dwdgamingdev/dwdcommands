package net.downwithdestruction.dwdcommands.commands;

import java.util.Random;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;

public class CmdKittycannon implements CommandExecutor {
	private static Random random = new Random();
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("kittycannon")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.kittycannon")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			EntityType cat = EntityType.OCELOT;
			Player p = (Player) cs;
			final Ocelot ocelot = (Ocelot)p.getWorld().spawn(p.getEyeLocation(), cat.getEntityClass());
			if (ocelot == null) { return true; }
			ocelot.setCatType(Ocelot.Type.values()[random.nextInt(Ocelot.Type.values().length)]);
			ocelot.setTamed(true);
			ocelot.setVelocity(p.getEyeLocation().getDirection().multiply(2));
			DwDCommands.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(DwDCommands.getInstance(), new Runnable() {
				@Override
				public void run() {
					final Location loc = ocelot.getLocation();
					ocelot.remove();
					loc.getWorld().createExplosion(loc, 0F);
				}
			}, 20);
			return true;
		}
		return false;
	}
}
