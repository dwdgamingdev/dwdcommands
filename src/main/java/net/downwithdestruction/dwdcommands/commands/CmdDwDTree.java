package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdDwDTree implements CommandExecutor {
	private void sendTreeList(Player p) {
		p.sendMessage(ChatColor.LIGHT_PURPLE + "Available Trees:");
		p.sendMessage(ChatColor.GRAY + "birch" + ChatColor.LIGHT_PURPLE + ", " +
				ChatColor.GRAY + "redwood" + ChatColor.LIGHT_PURPLE + ", " +
				ChatColor.GRAY + "tree" + ChatColor.LIGHT_PURPLE + ", " +
				ChatColor.GRAY + "redmushroom" + ChatColor.LIGHT_PURPLE + ", " +
				ChatColor.GRAY + "brownmushroom" + ChatColor.LIGHT_PURPLE + ", " +
				ChatColor.GRAY + "jungle" + ChatColor.LIGHT_PURPLE + ", " +
				ChatColor.GRAY + "junglebush" + ChatColor.LIGHT_PURPLE + ", " +
				ChatColor.GRAY + "swamp" + ChatColor.LIGHT_PURPLE + ", " +
				ChatColor.GRAY + "bigredwood" + ChatColor.LIGHT_PURPLE + ", " +
				ChatColor.GRAY + "bigtree" + ChatColor.LIGHT_PURPLE + ", " +
				ChatColor.GRAY + "bigjungle");
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("dwdtree")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.tree")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			TreeType tree;
			if (args.length < 1) {
				sendTreeList((Player) cs);
				return false;
			}
			else if (args[0].equalsIgnoreCase("birch"))			tree = TreeType.BIRCH;
			else if (args[0].equalsIgnoreCase("redwood"))		tree = TreeType.REDWOOD;
			else if (args[0].equalsIgnoreCase("tree"))			tree = TreeType.TREE;
			else if (args[0].equalsIgnoreCase("redmushroom"))	tree = TreeType.RED_MUSHROOM;
			else if (args[0].equalsIgnoreCase("brownmushroom"))	tree = TreeType.BROWN_MUSHROOM;
			else if (args[0].equalsIgnoreCase("jungle"))		tree = TreeType.SMALL_JUNGLE;
			else if (args[0].equalsIgnoreCase("junglebush"))	tree = TreeType.JUNGLE_BUSH;
			else if (args[0].equalsIgnoreCase("swamp"))			tree = TreeType.SWAMP;
			else if (args[0].equalsIgnoreCase("bigredwood"))	tree = TreeType.TALL_REDWOOD;
			else if (args[0].equalsIgnoreCase("bigtree"))		tree = TreeType.BIG_TREE;
			else if (args[0].equalsIgnoreCase("bigjungle"))		tree = TreeType.JUNGLE;
			else {
				sendTreeList((Player) cs);
				return false;
			}
			Player p = (Player) cs;
			Block bb = Utils.getTarget(p);
			if (bb == null) {
				cs.sendMessage(ChatColor.RED + "Can't create tree there!");
				return true;
			}
			Location bLoc = new Location(p.getWorld(), bb.getLocation().getX() + .5, bb.getLocation().getY() + 1, bb.getLocation().getZ() + .5, p.getLocation().getYaw(), p.getLocation().getPitch());
			final boolean success = p.getWorld().generateTree(bLoc, tree);
			if (success) {
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Created tree successfully.");
			} else {
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Failed to create tree.");
			}
			return true;
		}
		return false;
	}
}
