package net.downwithdestruction.dwdcommands.commands;

import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdStarve implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("starve")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.starve")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 1) {
				cmd.getDescription();
				return false;
			}
			Player t = DwDCommands.getInstance().getServer().getPlayer(args[0].trim());
			if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
				cs.sendMessage(ChatColor.RED + "That player is not online.");
				return true;
			}
			int toStarve;
			if (args.length > 1) {
				try {
					toStarve = Integer.parseInt(args[1]);
				} catch(NumberFormatException e) {
					cmd.getDescription();
					cs.sendMessage(ChatColor.RED + "Damage must be a number between 1 and 20.");
					return false;
				}
				if ((toStarve > 20) || (toStarve < 1)) {
					cs.sendMessage(ChatColor.RED + "The damage you entered is not between 1 and 20.");
					return true;
				}
			} else {
				toStarve = 5; // default amount to starve
			}
			if (Utils.isAuthorized(t, "dwdcommands.exempt.starve")) {
				cs.sendMessage(ChatColor.RED + "You cannot starve that player.");
				return true;
			}
			t.setFoodLevel(t.getFoodLevel() - toStarve);
			String name = (cs instanceof Player) ? ((Player) cs).getDisplayName() : "CONSOLE";
			t.sendMessage(ChatColor.LIGHT_PURPLE + "You have just been starved by " + ChatColor.GRAY + name + ChatColor.LIGHT_PURPLE + ".");
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "You just starved " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
			return true;
		}
		return false;
	}
}
