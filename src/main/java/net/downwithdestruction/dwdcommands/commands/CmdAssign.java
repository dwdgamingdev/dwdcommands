package net.downwithdestruction.dwdcommands.commands;

import java.util.ArrayList;

import net.downwithdestruction.dwdcommands.configuration.PlayerConfig;
import net.downwithdestruction.dwdcommands.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CmdAssign implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("assign")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.assign")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			if (args.length < 1) {
				Player p = (Player) cs;
				ItemStack hand = p.getItemInHand();
				if (hand == null || hand.getTypeId() == 0) {
					cs.sendMessage(ChatColor.RED + "You can't remove commands from air!");
					return true;
				}
				new PlayerConfig(p).set(null, "assign." + hand.getTypeId());
				p.sendMessage(ChatColor.LIGHT_PURPLE + "All commands removed from " + ChatColor.GRAY + hand.getType().toString().toLowerCase().replace("_", " ") + ChatColor.LIGHT_PURPLE + ".");
				return true;
			}
			Player p = (Player) cs;
			PlayerConfig pcm = new PlayerConfig(p);
			ItemStack hand = p.getItemInHand();
			if (hand == null || hand.getTypeId() == 0) {
				cs.sendMessage(ChatColor.RED + "You can't assign commands to air!");
				return true;
			}
			java.util.List<String> cmds = pcm.getStringList("assign." + hand.getTypeId());
			if (cmds == null) {
				cmds = new ArrayList<String>();
				cmds.add(Utils.getFinalArg(args, 0));
			} else cmds.add(Utils.getFinalArg(args, 0));
			pcm.setStringList(cmds, "assign." + hand.getTypeId());
			String message = (Utils.getFinalArg(args, 0).toLowerCase().startsWith("c:")) ? 
					ChatColor.LIGHT_PURPLE + "Added message " + ChatColor.GRAY + Utils.getFinalArg(args, 0).substring(2) + ChatColor.LIGHT_PURPLE + " to that item." : 
					ChatColor.LIGHT_PURPLE + "Added command " + ChatColor.GRAY + "/" + Utils.getFinalArg(args, 0) + ChatColor.LIGHT_PURPLE + " to that item.";
			p.sendMessage(message);
			return true;
		}
		return false;
	}
}
