package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.downwithdestruction.dwdcommands.configuration.PlayerConfig;
import net.downwithdestruction.dwdcommands.utils.Utils;
import net.downwithdestruction.dwdcommands.DwDCommands;

public class CmdBuddha implements CommandExecutor {
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("buddha")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.buddha")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length > 0) {
				if (!Utils.isAuthorized(cs, "dwdcommands.others.buddha")) {
					Utils.dispNoPerms(cs);
					return true;
				}
				Player t = DwDCommands.getInstance().getServer().getPlayer(args[0]);
				if ((t == null) || (DwDCommands.getInstance().isVanished(t))) {
					cs.sendMessage(ChatColor.RED + "That player does not exist!");
					return true;
				}
				PlayerConfig playerConfig = new PlayerConfig(t);
				if (!playerConfig.getBoolean("buddha")) {
					playerConfig.setBoolean(true, "buddha");
					t.sendMessage(ChatColor.LIGHT_PURPLE + "Buddha mode enabled by " + ChatColor.GRAY + ((Player) cs).getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
					cs.sendMessage(ChatColor.LIGHT_PURPLE + "Enabled buddha mode for " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
				} else {
					playerConfig.setBoolean(false, "buddha");
					t.sendMessage(ChatColor.LIGHT_PURPLE + "Buddha mode disabled by " + ChatColor.GRAY + ((Player) cs).getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
					cs.sendMessage(ChatColor.LIGHT_PURPLE + "Disabled buddha mode for " + ChatColor.GRAY + t.getDisplayName() + ChatColor.LIGHT_PURPLE + ".");
				}
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			Player p = (Player) cs;
			PlayerConfig playerConfig = new PlayerConfig(p);
			if (!playerConfig.getBoolean("buddha")) {
				playerConfig.setBoolean(true, "buddha");
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Enabled buddha mode for yourself.");
			} else {
				playerConfig.setBoolean(false, "buddha");
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "Disabled buddha mode for yourself.");
			}
			return true;
		}
		return false;
	}
}
