package net.downwithdestruction.dwdcommands.commands;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.configuration.PlayerConfig;
import net.downwithdestruction.dwdcommands.utils.Utils;

import java.util.Map;

public class CmdListHome implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("listhome")) {
			if (!Utils.isAuthorized(cs, "dwdcommands.listhome")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player) && args.length < 1) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			OfflinePlayer t;
			if (args.length < 1) {
				t = (OfflinePlayer) cs;
			} else {
				t = DwDCommands.getInstance().getServer().getPlayer(args[0]);
				if (t == null) t = DwDCommands.getInstance().getServer().getOfflinePlayer(args[0]);
				if (Utils.isAuthorized((CommandSender) t, "dwdcommands.exempt.listhome")) {
					cs.sendMessage(ChatColor.RED + "You can't list that player's homes!");
					return true;
				}
			}
			PlayerConfig pc = (args.length < 1) ? new PlayerConfig(cs.getName().toLowerCase()) : new PlayerConfig(args[0].trim().toLowerCase());
			if (!pc.exists()) {
				cs.sendMessage(ChatColor.RED + "No such player!");
				return true;
			}
			ConfigurationSection homelist = pc.getConfigurationSection("homes");
			if (homelist == null) {
				cs.sendMessage(ChatColor.RED + "No homes found!");
				return true;
			}
			final Map<String, Object> opts = homelist.getValues(false);
			if (opts.keySet().isEmpty()) {
				cs.sendMessage(ChatColor.RED + "No homes found!");
				return true;
			}
			String homes = opts.keySet().toString();
			homes = homes.substring(1, homes.length() - 1);
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "Homes:");
			cs.sendMessage(ChatColor.GRAY + homes);
			return true;
		}
		return false;
	}
}
