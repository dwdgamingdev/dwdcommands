package net.downwithdestruction.dwdcommands;

import com.griefcraft.lwc.LWC;
import com.griefcraft.lwc.LWCPlugin;
import java.util.ArrayList;
import java.util.List;
import net.downwithdestruction.dwdcommands.commands.*;
import net.downwithdestruction.dwdcommands.configuration.MainConfig;
import net.downwithdestruction.dwdcommands.listeners.*;
import net.downwithdestruction.dwdcommands.runners.*;
import net.downwithdestruction.dwdcommands.utils.Logger;
import net.downwithdestruction.dwdcommands.utils.Utils;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.kitteh.vanish.VanishPlugin;

public class DwDCommands extends JavaPlugin {
	public static DwDCommands instance;
	private final static Logger logger = Logger.getInstance();
	private final PluginManager pluginManager = Bukkit.getPluginManager();
	public static Permission permission = null;
	private VanishPlugin vp = null;
	public LWC lwc = null;
	
	public Boolean hasLWC = false;
	public Long afkKickTime = null;
	public Long afkAutoTime = null;
	public boolean kitPerms = false;
	public boolean showcommands = false;
	public boolean useWelcome = false;
	public boolean warpLastLocation = false;
	public String welcomeMessage = null;
	public String broadcastWelcomeMessage = null;
	public String kickMessage = null;
	public String banMessage = null;
	public String nickPrefix = null;
	public String defaultServerTitle = null;
	public String currentServerTitle = null;
	public List<String> muteCmds = new ArrayList<String>();
	public ConfigurationSection homeLimits = null;
	
	public DwDCommands() {
		instance = this;
	}
	
	public static DwDCommands getInstance() {
		return instance;
	}
	
	public void onEnable() {
		// Load Configuration
		loadConfig();
		
		// Get Dependencies
		vp = (VanishPlugin) getServer().getPluginManager().getPlugin("VanishNoPacket");
		if (pluginManager.isPluginEnabled("LWC")) {
			log(ChatColor.AQUA + "Detected LWC, enabling protections!");
			hasLWC = true;
			lwc = ((LWCPlugin) pluginManager.getPlugin("LWC")).getLWC();
		}
		
		// Set up Vault
		setupPermissions();
		
		// Register listeners
		pluginManager.registerEvents(new EntityListener(this), this);
		pluginManager.registerEvents(new PlayerListener(this), this);
		pluginManager.registerEvents(new MiscListener(this), this);
		
		
		// Register the commands
		registerCommands();
		
		logger.info("DwDCommands v" + this.getDescription().getVersion());
	}
	
        /**
        * @return overrides onDisabled
        */
        @Override
	public void onDisable() {
		getServer().getScheduler().cancelTasks(this);
		
		logger.info("DwDCommands Disabled.");
	}
	
	private void registerCommands() {
		this.getCommand("assign").setExecutor(new CmdAssign());
		this.getCommand("broadcast").setExecutor(new CmdBroadcast());
		this.getCommand("buddha").setExecutor(new CmdBuddha());
		this.getCommand("burn").setExecutor(new CmdBurn());
		this.getCommand("clearinventory").setExecutor(new CmdClearInventory());
		this.getCommand("coords").setExecutor(new CmdCoords());
		this.getCommand("depth").setExecutor(new CmdDepth());
		this.getCommand("enchant").setExecutor(new CmdEnchant());
		this.getCommand("enchantingtable").setExecutor(new CmdEnchantingTable());
		this.getCommand("enderchest").setExecutor(new CmdEnderChest());
		this.getCommand("feed").setExecutor(new CmdFeed());
		this.getCommand("fireball").setExecutor(new CmdFireball());
		this.getCommand("fly").setExecutor(new CmdFly());
		this.getCommand("furnace").setExecutor(new CmdFurnace());
		this.getCommand("gamemode").setExecutor(new CmdGamemode());
		this.getCommand("give").setExecutor(new CmdGive());
		this.getCommand("god").setExecutor(new CmdGod());
		this.getCommand("harm").setExecutor(new CmdHarm());
		this.getCommand("hat").setExecutor(new CmdHat());
		this.getCommand("heal").setExecutor(new CmdHeal());
		this.getCommand("invmod").setExecutor(new CmdInvmod());
		this.getCommand("jump").setExecutor(new CmdJump());
		this.getCommand("kill").setExecutor(new CmdKill());
		this.getCommand("killall").setExecutor(new CmdKillAll());
		this.getCommand("kit").setExecutor(new CmdKit());
		this.getCommand("kittycannon").setExecutor(new CmdKittycannon());
		this.getCommand("list").setExecutor(new CmdList());
		this.getCommand("listhome").setExecutor(new CmdListHome());
		this.getCommand("megasmite").setExecutor(new CmdMegaSmite());
		this.getCommand("nick").setExecutor(new CmdNick());
		this.getCommand("ping").setExecutor(new CmdPing());
		this.getCommand("quit").setExecutor(new CmdQuit());
		this.getCommand("ragequit").setExecutor(new CmdRageQuit());
		this.getCommand("repair").setExecutor(new CmdRepair());
		this.getCommand("ride").setExecutor(new CmdRide());
		this.getCommand("seen").setExecutor(new CmdSeen());
		this.getCommand("servertitle").setExecutor(new CmdServerTitle());
		this.getCommand("setspawn").setExecutor(new CmdSetSpawn());
		this.getCommand("smite").setExecutor(new CmdSmite());
		this.getCommand("spawn").setExecutor(new CmdSpawn());
		this.getCommand("speed").setExecutor(new CmdSpeed());
		this.getCommand("starve").setExecutor(new CmdStarve());
		this.getCommand("sudo").setExecutor(new CmdSudo());
		this.getCommand("suicide").setExecutor(new CmdSuicide());
		this.getCommand("taco").setExecutor(new CmdTaco());
		this.getCommand("thunder").setExecutor(new CmdThunder());
		this.getCommand("time").setExecutor(new CmdTime());
		this.getCommand("top").setExecutor(new CmdTop());
		this.getCommand("tppos").setExecutor(new CmdTpPos());
		this.getCommand("dwdtree").setExecutor(new CmdDwDTree());
		this.getCommand("weather").setExecutor(new CmdWeather());
		this.getCommand("whois").setExecutor(new CmdWhois());
		this.getCommand("workbench").setExecutor(new CmdWorkbench());
	}
	
	public boolean isVanished(Player p, CommandSender cs) {
		if (vp == null) {
			vp = (VanishPlugin) Bukkit.getServer().getPluginManager().getPlugin("VanishNoPacket");
			return false;
		} else  return !Utils.isAuthorized(cs, "dwdcommands.seehidden") && vp.getManager().isVanished(p.getName());
	}

        public boolean isVanished(Player t) {
		if (vp == null) {
			vp = (VanishPlugin) Bukkit.getServer().getPluginManager().getPlugin("VanishNoPacket");
			return false;
		} else  return vp.getManager().isVanished(t.getName());
        }
	
	private Boolean setupPermissions() {
		RegisteredServiceProvider<Permission> permissionProvider = this.getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
		if (permissionProvider != null) permission = permissionProvider.getProvider();
		return (permission != null);
	}
	
	private void loadConfig() {
		MainConfig conf = new MainConfig();
		kitPerms = conf.getBoolean("use-exclusive-kit-perms");
		useWelcome = conf.getBoolean("enable-welcome-message");
		warpLastLocation = conf.getBoolean("warp-last-location");
		welcomeMessage = Utils.colorize(conf.getString("welcome-message"));
		broadcastWelcomeMessage = Utils.colorize(conf.getString("broadcast-welcome-message"));
		kickMessage = Utils.colorize(conf.getString("kick-message"));
		banMessage = Utils.colorize(conf.getString("ban-message"));
		nickPrefix = Utils.colorize(getConfig().getString("nick-prefix"));
		muteCmds = conf.getStringList("mute-blocked-commands");
		showcommands = conf.getBoolean("log-all-commands");
		homeLimits = conf.getConfigurationSection("home-limits");
		currentServerTitle = conf.getString("current-server-title");
		defaultServerTitle = conf.getString("default-server-title");
	}
	
	public void log(String message) {
		
	}
	
	public void debug(String message) {
		
	}
}
